rm -rf ./allure/results/
behave -f allure_behave.formatter:AllureFormatter -o ./allure/results/ ./features/
allure serve ./allure/results/
