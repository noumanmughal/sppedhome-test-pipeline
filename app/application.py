from features.pages.launch_page import LaunchPage
from features.pages.login_page import LoginPage
from features.pages.main_page import MainPage
from features.pages.base_page import Page
from features.pages.home_page import HomePage
from features.pages.more_page import MorePage
from features.pages.profile_edit_page import ProfileEditPage
from features.pages.referfriend_page import ReferFriendPage
from features.pages.zero_deposit_eligibility_page import ZeroDepositEligibilityPage
from features.pages.create_listing_page import CreateListing
from features.pages.search_property_page import SearchProperty
from features.pages.favourite_page import FavouritePage
from features.pages.rental_collection_page import RentalCollection
from features.pages.tenant_page import TenantView
from features.pages.chat_button_page import ChatButtonPage
from features.pages.search_property_filters_page import SearchPropertyFilters


class Application:
    def __init__(self, driver):
        self.launch_page = LaunchPage(driver)
        self.login_page = LoginPage(driver)
        self.main_page = MainPage(driver)
        self.base_page = Page(driver)
        self.home_page = HomePage(driver)
        self.more_page = MorePage(driver)
        self.profile_edit_page = ProfileEditPage(driver)
        self.refer_friend_page = ReferFriendPage(driver)
        self.zero_deposit_eligibility_page = ZeroDepositEligibilityPage(driver)
        self.create_listing = CreateListing(driver)
        self.search_property = SearchProperty(driver)
        self.favourite_page = FavouritePage(driver)
        self.rental_collection = RentalCollection(driver)
        self.tenant_view = TenantView(driver)
        self.chat_button_page = ChatButtonPage(driver)
        self.search_property_filters = SearchPropertyFilters(driver)
