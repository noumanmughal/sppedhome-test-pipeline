from appium import webdriver
from app.application import Application
from allure_commons._allure import attach
from allure_commons.types import AttachmentType
import os
import glob
import subprocess


def before_scenario(context, scenario):
    launch_app(context, scenario)


def after_scenario(context, scenario):
    context.driver.quit()


def after_step(context, step):
    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )


def push_profile_pictures(context):
    dir_path = os.getcwd() + "\\resources\\images\\profile\\*"
    all_files = glob.glob(dir_path)
    for items in all_files:
        print(items)


def launch_app(context, scenario):
    app_name = "speedhome.apk"
    app_path = os.getcwd() + "\\resources\\" + app_name
    url = "http://127.0.0.1:4723/wd/hub"

    context.driver = webdriver.Remote(url,
                                      desired_capabilities={'platformName': 'Android',
                                                            'autoGrantPermissions': True,
                                                            'deviceName': 'emulator-5554',
                                                            'app': f"{app_path}"
                                                            })

    context.driver.implicitly_wait(60)

    context.app = Application(context.driver)

