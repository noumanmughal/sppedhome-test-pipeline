@registration
Feature: Registration

  Scenario: Reg01 - Verify that user is able to launch the App.
    Given App is installed
    When The app is launched
    Then Login screen is shown

  Scenario: Reg02 - Verify that on clicking the login button option to login through mobile is shown
    Given Login screen is shown
    When Login button is clicked
    Then Login through mobile number screen is shown

  Scenario: Reg03 - Verify that on clicking next button welcome modal is shown to user.
    Given Enter your mobile number screen is shown
    When Valid mobile number is entered
    And Next button is clicked
    Then Welcome modal is shown

  Scenario: Reg04 - Verify that on clicking request OTP button , OTP modal is shown to user.
    Given Welcome modal screen is shown
    When Request 4 digit OTP button is clicked
    Then OTP modal is shown

  Scenario: Reg08 - Verify the profile found action if profile found.
    Given Enter your mobile number screen is shown
    When Valid registered mobile number is entered
    And Next button is clicked
    Then Welcome modal with Login with password button is shown

  Scenario: Reg09 - Verify that the Text "Please enter your mobile number below" is on screen
    Given Login screen is shown
    When Login button is clicked
    Then Mobile number text-field shows Your Mobile Number placeholder text

  Scenario: Reg12 - Verify user can re-request OTP
    Given OTP modal screen is shown
    When OTP timer expires
    Then Resend OTP code button with Didn't get the code text is shown
    And Clicking on Resend button resends the code

  Scenario: Reg15 - Verify that length validation is applied on code verification field
    Given OTP modal screen is shown
    When "123456" OTP code is entered
    Then Only 4 digits of "123456" otp code is accepted


  Scenario: Test - Testing
#    Given Enter your mobile number screen is shown
    Given I do testing



