@zerodepositeligibility
Feature: Zero Deposit Eligibility Page

  Scenario: ZDE01 - Verify that user is shown and able to submit zero deposit eligibility from MobileApp
    Given User is on the more page
    When Edit profile button is clicked
    Then Check now or re-submit button is shown

  Scenario: ZDE02 - Verify that User is able to Click on buttons shown
    Given User is on ZDE page
    When Check now or re-submit button is clicked
    Then Two citizenship buttons are shown

  Scenario: ZDE04 - Verify that user is able to upload information on clicking “I am Malaysian Citizen” related fields are shown.
    Given User is on select citizenship for zde page
    When I am Malaysian Citizen button is clicked
    Then All the related fields for malaysian citizen are shown

  Scenario: ZDE05 - Verify that if textfields are empty error is shown to user.
    Given User is on select citizenship for zde page
    And I am Malaysian Citizen button is clicked
    When Submit button is clicked without entering any data
    Then Error regarding required field is shown

  Scenario: ZDE06 - Verify that check box is shown.
    Given User is on select citizenship for zde page
    When I am Malaysian Citizen button is clicked
    Then Acknowledgement checkbox is shown

  Scenario: ZDE07 - Verify that submit button is shown..
    Given User is on select citizenship for zde page
    When I am Malaysian Citizen button is clicked
    Then Submit button is shown

  Scenario: ZDE08 - Verify that user is able to upload docs on clicking “I am Foreigner” button upload document section is shown to user.
    Given User is on select citizenship for zde page
    When I am Foreigner button is clicked
    And Next button on Get Verified screen is clicked
    Then Upload document section for foreign citizen is shown

  Scenario: ZDE09 - Verify that three options are shown to user. .
    Given User is on select citizenship for zde page
    When I am Foreigner button is clicked
    And Next button on Get Verified screen is clicked
    Then Select you profession options are shown

  Scenario: ZDE10 - Verify that on selecting any option data fields related to option is shown.
    Given User is on select citizenship for zde page
    When I am Foreigner button is clicked
    And Next button on Get Verified screen is clicked
    Then Upload files section for foreign citizen is shown


