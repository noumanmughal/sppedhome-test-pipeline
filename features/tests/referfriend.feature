@referfriend
Feature: Refer A Friend Page

  Scenario: RF01 - Verify Clicking on Refer a friend Refer page is shown
    Given User is on the more page
    When Refer A Friend button is clicked
    Then Referral link is shown
