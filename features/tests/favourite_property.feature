@favourite
Feature: Favourite Property

  Scenario: FAV01 - Verify that users is able to add any property to favourites
    Given User is signed in
    When A property is added as favourite
    Then Added favourite property shows under favourite listing

  Scenario: FAV01 - Verify that users is able to remove any property from favourites
    Given User is signed in
    When A property is removed from favourite
    Then Removed favourite property does not show under favourite listing
