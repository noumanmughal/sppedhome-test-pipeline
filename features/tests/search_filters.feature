@searchfilters
Feature: Search Filters

  Scenario: SF01 - Verify that user is able to search the listing and listing is shown as per requirement
    Given User is signed in
    When Home button is clicked
    Then Search bar with rent and buy options shows

  Scenario: SF02 - Verify that user is able to write the desired area in the search bar
    Given User is signed in
    And I search for properties in "kuala lumpur"
    When "kuala lumpur" properties are shown
    And Search bar is opened again after going back
    Then Recent searches shows in searchbar

  Scenario: SF03 - Verify that user is able to Filter the properties
    Given User is on search result page
    When Filters button is clicked
    Then Filters page with all the filter buttons show

  Scenario: SF04 - Verify that user is able to filter the search results for properties as per desired
    Given User is on search result page
    When Filters button is clicked
    Then Filters page with all the filter buttons show

