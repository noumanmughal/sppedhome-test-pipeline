@searchproperty
Feature: Search Property

  Scenario: SP01 - Verify that user is able to search the listing and listing is shown as per requirement
    Given User is signed in
    When Home button is clicked
    Then Search bar with rent and buy options shows

  Scenario: SP02 - Verify that When user search for the property
    Given User is signed in
    When I search for properties in "kuala lumpur"
    Then "kuala lumpur" properties are shown

  Scenario: SP03 - Verify that user is able to select the options of Rent or Buy from the search bar
    Given User is signed in
    When Buy button on search dropdown is clicked
    Then "Selling" properties are shown

  Scenario: SP04 - Verify that user is able to select the options of Rent or Buy from the search bar
    Given User is signed in
    When Rent button on search dropdown is clicked
    Then "Renting" properties are shown

  Scenario: SP05 - Verify that When user search for the property
    Given User is signed in
    When I search for properties in "kuala lumpur"
    And "kuala lumpur" properties are shown
    Then Sort,Search Filter and Map options should be shown

  Scenario: SP06 - Verify that user is able to Sort the properties using Sort filter
    Given User is signed in
    When I search for properties in "kuala lumpur"
    And "kuala lumpur" properties are shown
    Then Sort option should be shown

  Scenario: SP07 - Verify that user is able to Sort the properties using Relevance, Distance and Price options
    Given User is on search result page
    When Sort filter button is clicked
    Then Sort pop-up with sort options are shown

  Scenario: SP08 - Verify that user is able to Filter the properties
    Given User is on search result page
    When Filters button is clicked
    Then Filters page with all the filter buttons show

  Scenario: SP09 - Verify that user is able to Filter the properties
    Given User is on search result page
    When Map button is clicked
    Then Map view with properties listed is shown
