@morepage
Feature: More Page

  Scenario: M01 - Verify that More page is shown to user
    Given User is signed in
    When More button is clicked
    Then More page with the related options show

#  Scenario: M99 - TESTING
#    Given User is signed in
#    Then I do testing
