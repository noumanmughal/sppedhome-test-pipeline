@logout
Feature: Logout

  Scenario: LO01 - Verify that User should logout by clicking logout button.
    Given User is on the more page
    When Log out button is clicked
    And Ok button is clicked on the confirmation box
    Then Login button is shown on more page
