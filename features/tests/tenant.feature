@tenant
Feature: Tenant View page

  Scenario: TV01 - Verify that on clicking property , property detail page is shown to user.
    Given User is signed in
    When A property card is clicked
    Then Property details are shown

  Scenario: TV02 - Verify that chat button is shown to user.
    Given User is signed in
    When A property card is clicked
#    Then Chat button is shown