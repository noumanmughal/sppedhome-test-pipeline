@rentalcollection
Feature: Rental Collection PAge

  Scenario: RC01 - Verify that Invoice and bills is shown to the tenant
    Given User is on More Page after login with valid otp
    When Rental Collection button is clicked
    And A rental collection record is clicked
    Then Rental Collection details are shown
