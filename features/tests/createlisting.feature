@createlisting
Feature: Create Listing Page

  Scenario: CL01 - Verify that Add Button is shown in top of the App
    Given User is on properties page
    And Add Properties button is clicked
    Then Property type selection options are shown

  Scenario: CL02 - Verify that Rent Property is successfully posting
    Given User is on properties page
    And Add Properties button is clicked
    When Rent Property button is clicked
    Then All rent property options are shown

  Scenario: CL03 - Verify that Sell Property is successfully listing/posting
    Given User is on properties page
    And Add Properties button is clicked
    When Sell Property button is clicked
    Then All sell property options are shown

  Scenario: CL04 - Verify that user is able to Edit the property listed
    Given User is on Active Listing properties page
    When Edit Listing button on the first post is clicked
    Then All Edit Listing related fields are shown

  Scenario: CL05 - Verify that user is able to archieve the property listed
    Given User is on Active Listing properties page
    When Archive button on the first post is clicked
    Then Archived Property post is shown under Archive Listing

