@login
Feature: Login

  Scenario: Login01 - Verify that on clicking the login button option to login through mobile is shown.
    Given Login screen is shown
#    When Login button is clicked
#    Then Login through mobile number screen is shown

  Scenario: Login03 - Verify that textbox for entering number is shown to user.
    Given Login screen is shown
    And Login button is clicked
    Then Enter mobile number text-field is shown

  Scenario: Login04 - Verify that on clicking login/signup button OTP modal is shown
    Given Welcome modal screen is shown
    When Request 4 digit OTP button is clicked
    Then OTP modal is shown

  Scenario: Login05 - Verify that user is able to login with valid OTP
    Given Enter your mobile number screen is shown
    And Valid registered mobile number is entered
    And Next button is clicked
    When Request 4 digit OTP button is clicked
    And Valid otp code is entered
    Then Home screen is shown after successful login

  Scenario: Login07 - Verify that user is not able to login with invalid code
    Given OTP modal screen is shown
    When Invalid otp code is entered
    Then Invalid OTP number entered message is shown

  Scenario: Login08 - Verify that user is able to login with Password
    Given Welcome modal screen for registered phone number is shown
    And Login with password button is clicked
    When Valid password is entered
    And Next button on enter password screen is clicked
    Then Home screen is shown after successful login

  Scenario: Login09 - Verify that user is able to use forget password to make new password
    Given Login with password modal is shown
    When Forgot password button is clicked
    Then OTP modal is shown

  Scenario: Login10 - Verify that fields after shown is editable for new password
    Given Login with password screen is shown
    When Forgot password button is clicked
    And Valid otp code is entered
    Then Reset Password screen is shown
