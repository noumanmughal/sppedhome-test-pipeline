@profile
Feature: Profile

  Scenario: PM01 - Verify that user is able to access profile section, and it contains the related options.
    Given User is signed in
    When More button is clicked
    And Edit profile button is clicked
    Then Edit profile screen is shown

  Scenario: UP01 - Verify that user is able to access profile section, and it contains the related options and fields.
    Given User is signed in
    When More button is clicked
    And Edit profile button is clicked
    Then Edit profile screen show all options

  Scenario: UP02 - Verify that user is able to upload valid profile image
    Given User is on the more page
    When Profile picture is updated
    Then New profile picture shows

  Scenario: UP04 - Verify that user is able to update any fields
    Given User is on edit profile page
    When Some profile fields are updated
    Then Updated fields with updated data show