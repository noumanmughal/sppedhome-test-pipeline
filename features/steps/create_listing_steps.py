import time
from features.credentials import *
from behave import given, when, then, step


@step('User is on properties page')
def step_impl(context):
    context.execute_steps(u"""
    Given User is signed in
    And Properties button is clicked
    """)


@step('User is on Active Listing properties page')
def step_impl(context):
    context.execute_steps(u"""
    Given User is signed in
    And Properties button is clicked
    Then Active Listing button is clicked
    """)


@step('Add Properties button is clicked')
def step_impl(context):
    context.app.create_listing.click_on_add_property_btn()


@step('Sell Property button is clicked')
def step_impl(context):
    context.app.create_listing.click_sell_property_btn()


@step('Rent Property button is clicked')
def step_impl(context):
    context.app.create_listing.click_rent_property_btn()


@step('Property type selection options are shown')
def step_impl(context):
    does_rent_sell_btn_show = context.app.create_listing.does_rent_sell_btn_show()
    assert does_rent_sell_btn_show == True, "Property types are not showing"


@step('All rent property options are shown')
def step_impl(context):
    does_all_rent_property_options_show = context.app.create_listing.does_all_rent_property_options_show()
    assert does_all_rent_property_options_show == True, "Rent Properties options are not showing"


@step('All sell property options are shown')
def step_impl(context):
    does_all_sell_property_options_show = context.app.create_listing.does_all_sell_property_options_show()
    assert does_all_sell_property_options_show == True, "Sell Properties options are not showing"


# @step('Edit Listing button is clicked')
# def step_impl(context):
#     context.app.create_listing.click_edit_listing_btn()


@step('Active Listing button is clicked')
def step_impl(context):
    context.app.create_listing.click_active_listing_btn()


@step('Edit Listing button on the first post is clicked')
def step_impl(context):
    context.app.create_listing.click_edit_listing_btn()


@step('All Edit Listing related fields are shown')
def step_impl(context):
    does_all_edit_listing_fields_show = context.app.create_listing.does_edit_property_all_fields_show()
    assert does_all_edit_listing_fields_show == True, "All edit listing fields are not showing"


@step('Archive button on the first post is clicked')
def step_impl(context):
    context.app.create_listing.click_archive_listing_btn()
    context.app.create_listing.click_ok_on_archive_status_popup()


@step('Archived Property post is shown under Archive Listing')
def step_impl(context):
    does_archive_property_show_inactive= context.app.create_listing.does_archived_property_shows_inactive()
    assert does_archive_property_show_inactive == True, "Archived Property is not showing under Inactive Listings"





