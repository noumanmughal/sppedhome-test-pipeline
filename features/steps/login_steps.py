import time
from features.credentials import *
from behave import given, when, then, step
from features.pages.base_page import Page


@then("Login through mobile number screen is shown")
def login_via_number_screen(context):
    title_txt_present = context.app.login_page.is_login_screen_title_present()
    assert title_txt_present == True, "Enter Your Mobile Number Title text is not present"

    country_code_txtfield_present = context.app.login_page.is_country_code_txtfield_present()
    assert country_code_txtfield_present == True, "Country Code textfield is not present"

    mobile_number_txtfield_present = context.app.login_page.is_mobile_numbertxtfielld_present()
    assert mobile_number_txtfield_present == True, "Your Mobile Number textfield is not present"

    next_btn_present = context.app.login_page.is_next_btn_present()
    assert next_btn_present == True, "Next Button is not present"


@step("Valid mobile number is entered")
def enter_unreg_mobile_number(context):
    context.app.login_page.enter_unreg_mobile_number(REGISTERED_MOBILE_NUMBER)


@step("Registered mobile number is entered")
def step_impl(context):
    context.app.login_page.enter_unreg_mobile_number(REGISTERED_MOBILE_NUMBER2)


@step("Next button is clicked")
def click_next_button(context):
    context.app.login_page.click_next_btn()


@step("Welcome Modal is shown")
def welcome_modal_shown(context):
    is_modal_shown = context.app.login_page.is_unreg_welcome_modal_shown()
    assert is_modal_shown == True, "Welcome Modal is not showing"


@given("Welcome modal screen is shown")
def step_impl(context):
    context.execute_steps(u"""
      Given Login screen is shown
      And Login button is clicked
      When Valid mobile number is entered
      And Next button is clicked
      Then Welcome modal is shown
    """)


@given("Welcome modal screen for registered phone number is shown")
def step_impl(context):
    context.execute_steps(u"""
      Given Login screen is shown
      And Login button is clicked
      When Registered mobile number is entered
      And Next button is clicked
      Then Welcome modal is shown
    """)


@given("Login with password modal is shown")
def step_impl(context):
    context.execute_steps(u"""
      Given Welcome modal screen for registered phone number is shown
      Then Login with password button is clicked
    """)


@given("Login with password screen is shown")
def step_impl(context):
    context.execute_steps(u"""
      Given Welcome modal screen is shown
      Then Login with password button is clicked
    """)


@when("Request 4 digit OTP button is clicked")
def step_impl(context):
    context.app.login_page.click_on_request_otp()


@step("Login with password button is clicked")
def step_impl(context):
    context.app.login_page.click_on_login_with_password()


@step("Next button on enter password screen is clicked")
def step_impl(context):
    context.app.login_page.click_on_enter_password_next_btn()


@step("Forgot password button is clicked")
def step_impl(context):
    context.app.login_page.click_on_forgot_password_btn()


@then("OTP modal is shown")
def step_impl(context):
    is_otp_modal_shown = context.app.login_page.is_otp_modal_shown()
    assert is_otp_modal_shown == True, "One Time Password modal is not showing"


@step("OTP modal screen is shown")
def step_impl(context):
    context.execute_steps(u"""
    Given Welcome modal screen is shown
    When Request 4 digit OTP button is clicked
    Then OTP modal is shown
    """)


@when("Valid otp code is entered")
def step_impl(context):
    context.app.login_page.enter_otp(OTP_CODE)


@when("Valid password is entered")
def step_impl(context):
    context.app.login_page.enter_password(PASSWORD)


@when("Invalid otp code is entered")
def step_impl(context):
    otp_code = Page.random_number(context, 4)
    context.app.login_page.enter_otp(otp_code)


@then("Enter mobile number text-field is shown")
def step_impl(context):
    is_mobile_txtfield_present = context.app.login_page.is_mobile_numbertxtfielld_present()
    assert is_mobile_txtfield_present == True, "Enter mobile number text-field is not showing"


@given("Enter your mobile number screen is shown")
def step_impl(context):
    context.execute_steps(u"""
    Given Login screen is shown
    And Login button is clicked
    Then Login through mobile number screen is shown
    """)


@step("Valid registered mobile number is entered")
def step_impl(context):
    context.app.login_page.enter_registered_mobile_number(REGISTERED_MOBILE_NUMBER)


@then("Welcome modal with Login with password button is shown")
def step_impl(context):
    is_otp_modal_shown = context.app.login_page.is_reg_welcome_modal_shown()
    assert is_otp_modal_shown == True, "One Time Password modal is not showing"


@then("Mobile number text-field shows Your Mobile Number placeholder text")
def step_impl(context):
    does_mobile_number_has_placeholder_txt = context.app.login_page.does_mobile_number_has_placeholder_txt()
    assert does_mobile_number_has_placeholder_txt == True, "Mobile number field does not have Your Mobile Number " \
                                                           "placeholder text "


@step("I do testing")
def step_impl(context):
    # context.app.home_page.show_details()
    files = context.app.base_page.send_profile_pictures_to_emulator()
    print(files)


@when("OTP timer expires")
def step_impl(context):
    is_otp_timer_present = context.app.login_page.is_otp_timer_present()
    assert is_otp_timer_present == True, "OTP timer is not present"


@then("Resend OTP code button with Didn't get the code text is shown")
def step_impl(context):
    is_didnt_get_code_txt_present = context.app.login_page.is_didnt_get_code_txt_present()
    assert is_didnt_get_code_txt_present == True, "Didn't get the code? text is not present"

    is_resend_btn_present = context.app.login_page.resend_btn_present()
    assert is_resend_btn_present == True, "Didn't get the code? Resend button is not present"


@step("Clicking on Resend button resends the code")
def step_impl(context):
    context.app.login_page.click_on_resend_btn()

    is_otp_sent_toast_msg_present = context.app.login_page.is_otp_sent_toast_message_present()
    assert is_otp_sent_toast_msg_present == True, "OTP sent successfully toast message is not present"

    is_otp_timer_present = context.app.login_page.is_otp_timer_present()
    assert is_otp_timer_present == True, "OTP timer is not present"


@step('"{otp_code}" OTP code is entered')
def step_impl(context, otp_code):
    context.app.login_page.enter_otp(otp_code)


@step('Only 4 digits of "{otp_code}" otp code is accepted')
def step_impl(context, otp_code):
    entered_otp_code = otp_code
    accepted_otp_code = context.app.login_page.accepted_otp_code()

    assert entered_otp_code != accepted_otp_code, "Entered OTP and Accepted OTP are same"
    assert len(entered_otp_code) != len(accepted_otp_code), "Length of Entered OTP and Accepted is same"


@then("Invalid OTP number entered message is shown")
def step_impl(context):
    is_invalid_otp_err_present = context.app.login_page.is_invalid_otp_err_present()
    assert is_invalid_otp_err_present == True, "Invalid OTP numbers entered. Try again. error not present"


@then("Reset Password screen is shown")
def step_impl(context):
    is_reset_password_screen_shown = context.app.login_page.is_reset_password_screen_shown()
    assert is_reset_password_screen_shown == True, "Reset Password screen is not showing"


# @given("User is signed in")
# def step_impl(context):
#     context.execute_steps(u"""
#     Given Welcome modal screen for registered phone number is shown
#     And Login with password button is clicked
#     When Valid password is entered
#     And Next button on enter password screen is clicked
#     Then Home screen is shown after successful login
#     """)

@given("User is signed in")
def step_impl(context):
    context.execute_steps(u"""
    Given Enter your mobile number screen is shown
    And Valid registered mobile number is entered
    And Next button is clicked
    When Request 4 digit OTP button is clicked
    And Valid otp code is entered
    Then Home screen is shown after successful login
    """)
