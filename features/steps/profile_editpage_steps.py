import time
from features.credentials import *
from behave import given, when, then, step


@step('Edit profile screen is shown')
def step_impl(context):
    does_edit_profile_page_show = context.app.profile_edit_page.does_edit_profile_page_show()
    assert does_edit_profile_page_show == True, "Edit profile page is not showing"


@step('Edit profile screen show all options')
def step_impl(context):
    does_edit_profile_page_show = context.app.profile_edit_page.does_edit_profile_page_with_all_info_show()
    assert does_edit_profile_page_show == True, "Edit profile page is not showing"


@step("User is on edit profile page")
def step_impl(context):
    context.execute_steps(u"""
    Given User is signed in
    When More button is clicked
    And Edit profile button is clicked
    Then Edit profile screen is shown
    """)


@step('Profile picture is updated')
def step_impl(context):
    context.app.profile_edit_page.change_profile_pic()


@step('New profile picture shows')
def step_impl(context):
    is_pp_updated = context.app.profile_edit_page.is_profile_picture_updated()
    context.app.profile_edit_page.remove_saved_screenshots()
    assert is_pp_updated == True, "Profile picture is not updating"


@step('Some profile fields are updated')
def step_impl(context):
    context.app.profile_edit_page.change_profile_details()


@step('Updated fields with updated data show')
def step_impl(context):
    are_fields_updated = context.app.profile_edit_page.is_profile_details_updated()
    assert are_fields_updated == True, "Profile details fields are not updating after entering new data"


