import time
from features.credentials import *
from behave import given, when, then, step


@step('User is on the more page')
def step_impl(context):
    context.execute_steps(u"""
    Given User is signed in
    And More button is clicked
    """)


@step('Log out button is clicked')
def step_impl(context):
    context.app.more_page.click_on_logout_btn()


@step('Ok button is clicked on the confirmation box')
def step_impl(context):
    context.app.more_page.click_ok_confirmation_box()


@step('Login button is shown on more page')
def step_impl(context):
    does_login_btn_show = context.app.more_page.is_login_btn_present()
    assert does_login_btn_show == True, "Logout was unsuccessful, login button is not present"







