import time
from features.credentials import *
from behave import given, when, then, step


@step('Property details are shown')
def step_impl(context):
    does_property_detail_show = context.app.tenant_view.does_property_details_show()
    assert does_property_detail_show == True, "Property details are not showing"


@step('Chat button is shown')
def step_impl(context):
    does_chat_btn_show = context.app.tenant_view.is_chat_btn_present()
    assert does_chat_btn_show == True, "Chat Button is not showing"







