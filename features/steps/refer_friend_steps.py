import time
from features.credentials import *
from behave import given, when, then, step


@step('Refer A Friend button is clicked')
def step_impl(context):
    context.app.more_page.click_refer_friend_btn()


@step('Referral link is shown')
def step_impl(context):
    does_referral_link_exists = context.app.refer_friend_page.does_refer_link_show()
    assert does_referral_link_exists == True, "Referral link does not exist"







