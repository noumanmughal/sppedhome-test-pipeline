import time
from features.credentials import *
from behave import given, when, then, step


@step('Book Virtual Appointment button is clicked on a property details page')
def step_impl(context):
    context.app.chat_button_page.click_book_virtual_appointment_btn()


@step('Chat request modal is shown')
def step_impl(context):
    does_chat_request_modal_show = context.app.chat_button_page.does_chat_request_modal_shows()
    assert does_chat_request_modal_show == True, "Chat request modal is not showing"


@step('Viewing Time submit button is clicked')
def step_impl(context):
    context.app.chat_button_page.click_viewing_time_submit_button()


@step('Additional info modal is shown')
def step_impl(context):
    does_ai_modal_show = context.app.chat_button_page.does_additional_info_modal_shows()
    assert does_ai_modal_show == True, "Additional Info modal is not showing"


@step('Save changes button is shown')
def step_impl(context):
    does_ai_modal_show = context.app.chat_button_page.is_ai_save_exit_btn_present()
    assert does_ai_modal_show == True, "Save and exit button is not showing"

