import time
from features.credentials import *
from behave import given, when, then, step


@step('Search bar with rent and buy options shows')
def step_impl(context):
    does_searchbar_with_all_options_show = context.app.home_page.is_searchbar_with_all_options_present()
    assert does_searchbar_with_all_options_show == True, "Searchbar with Rent and Buy options is not showing"


@step('Buy button on search dropdown is clicked')
def step_impl(context):
    context.app.home_page.click_search_buy_btn()


@step('Rent button on search dropdown is clicked')
def step_impl(context):
    context.app.home_page.click_search_rent_btn()


@step('"{property_type}" properties are shown')
def step_impl(context, property_type):
    does_selling_properties_show = context.app.search_property.does_search_return_results()
    assert does_selling_properties_show == True, "Selling properties are not showing"


@step('I search for properties in "{address}"')
def step_impl(context, address):
    context.app.search_property.property_search(address)


@step('Sort,Search Filter and Map options should be shown')
def step_impl(context):
    does_search_options_show = context.app.search_property.does_search_options_show()
    assert does_search_options_show == True, "Sort,Search Filter and Map options are not showing"


@step('Sort option should be shown')
def step_impl(context):
    does_sort_options_show = context.app.search_property.is_sort_btn_present()
    assert does_sort_options_show == True, "Sort option is not showing"


@step('User is on search result page')
def step_impl(context):
    context.execute_steps(u"""
    Given User is signed in
    When I search for properties in "{address}"
    Then "{property_type}" properties are shown
    """.format(address="kuala lumpur", property_type="kuala lumpur"))


@step('Sort filter button is clicked')
def step_impl(context):
    context.app.search_property.click_sort_filter_btn()


@step('Sort pop-up with sort options are shown')
def step_impl(context):
    does_sort_options_show = context.app.search_property.does_sort_pop_up_with_options_show()
    assert does_sort_options_show == True, "Sort pop-up with options is not showing"


@step('Filters button is clicked')
def step_impl(context):
    context.app.search_property.click_filters_btn()


@step('Search bar is opened again after going back')
def step_impl(context):
    context.app.search_property.click_back_btn_on_search_results()
    context.app.search_property.open_search_bar()


@step('Filters page with all the filter buttons show')
def step_impl(context):
    does_price_bar_shows = context.app.search_property_filters.is_filter_pricebar_present()
    assert does_price_bar_shows == True, "Pricebar is not showing on filters page"

    does_housing_type_filters_shows = context.app.search_property_filters.is_housing_type_filter_btns_present()
    assert does_housing_type_filters_shows == True, "Housing Type filter buttons are not showing on filters page"

    does_furnish_level_filters_shows = context.app.search_property_filters.is_title_furnish_level_filter_btns_present()
    assert does_furnish_level_filters_shows == True, "Furnish Type filter buttons are not showing on filters page"

    does_rooms_filters_shows = context.app.search_property_filters.is_rooms_filter_btns_present()
    assert does_rooms_filters_shows == True, "Rooms filter buttons are not showing on filters page"

    does_bathrooms_filters_shows = context.app.search_property_filters.is_bathrooms_filter_btns_present()
    assert does_bathrooms_filters_shows == True, "Bathrooms filter buttons are not showing on filters page"

    does_car_parks_filters_shows = context.app.search_property_filters.is_car_parks_filter_btns_present()
    assert does_car_parks_filters_shows == True, "Car Parks filter buttons are not showing on filters page"

    does_ei_filters_shows = context.app.search_property_filters.is_extra_info_filter_btns_present()
    assert does_ei_filters_shows == True, "Extra Info filter buttons are not showing on filters page"

    does_filter_now_btn_shows = context.app.search_property_filters.is_filter_now_btn_present()
    assert does_filter_now_btn_shows == True, "Filter Now button is not showing on filters page"

    does_reset_filter_btn_shows = context.app.search_property_filters.is_reset_filter_btn_present()
    assert does_reset_filter_btn_shows == True, "Reset Filter button is not showing on filters page"


@step('Map button is clicked')
def step_impl(context):
    context.app.search_property.click_map_btn()


@step('Map view with properties listed is shown')
def step_impl(context):
    does_map_view_shows = context.app.search_property_filters.does_locations_on_map_shows()
    assert does_map_view_shows == True, "Map View with properties listed is not showing"


@step('Recent searches shows in searchbar')
def step_impl(context):
    does_recent_searches_show = context.app.search_property_filters.does_recent_searches_show()
    assert does_recent_searches_show == True, "Recent Searches are not showing in the searach bar"
