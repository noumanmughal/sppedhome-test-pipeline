import time
from features.credentials import *
from behave import given, when, then, step


@step('Check now or re-submit button is shown')
def step_impl(context):
    are_btns_present = context.app.more_page.is_checknow_or_resubmit_btn_present()
    assert are_btns_present == True, "Both check now and re-submit buttons are not showing"


@step('Check now or re-submit button is clicked')
def step_impl(context):
    context.app.more_page.click_checknow_or_resubmit_btn()


@step('User is on ZDE page')
def step_impl(context):
    context.execute_steps(u"""
    Given User is on the more page
    Then Edit profile button is clicked
    """)


@step('User is on select citizenship for zde page')
def step_impl(context):
    context.execute_steps(u"""
    Given User is on the more page
    When Edit profile button is clicked
    Then Check now or re-submit button is clicked
    """)


@step('Two citizenship buttons are shown')
def step_impl(context):
    context.app.zero_deposit_eligibility_page.does_citizenship_btn_show()


@step('I am Malaysian Citizen button is clicked')
def step_impl(context):
    context.app.zero_deposit_eligibility_page.click_on_malaysian_citizen_btn()


@step('I am Foreigner button is clicked')
def step_impl(context):
    context.app.zero_deposit_eligibility_page.click_on_foreigner_citizen_btn()


@step('Next button on Get Verified screen is clicked')
def step_impl(context):
    context.app.zero_deposit_eligibility_page.click_on_get_verified_next_btn()


@step('All the related fields for malaysian citizen are shown')
def step_impl(context):
    does_all_related_fields_show = context.app.zero_deposit_eligibility_page.does_malaysian_citizen_zde_details_show()
    assert does_all_related_fields_show == True, "Malaysian Citizen related fields are not showing"


@step('Upload document section for foreign citizen is shown')
def step_impl(context):
    does_all_related_fields_show = context.app.zero_deposit_eligibility_page.does_upload_section_for_foreigner_show()
    assert does_all_related_fields_show == True, "Upload Document section for Foreigner is not showing"


@step('Submit button is clicked without entering any data')
def step_impl(context):
    context.app.zero_deposit_eligibility_page.click_submit_btn()


@step('Error regarding required field is shown')
def step_impl(context):
    does_name_error_show = context.app.zero_deposit_eligibility_page.is_name_required_error_present()
    assert does_name_error_show == True, "Name required error is not showing"


@step('Acknowledgement checkbox is shown')
def step_impl(context):
    is_checkbox_present = context.app.zero_deposit_eligibility_page.is_acknowledgement_checkbox_present()
    assert is_checkbox_present == True, "Acknowledgement checkbox is not showing"


@step('Submit button is shown')
def step_impl(context):
    is_checkbox_present = context.app.zero_deposit_eligibility_page.is_submit_btn_present()
    assert is_checkbox_present == True, "Submit button is not showing"


@step('Select you profession options are shown')
def step_impl(context):
    does_profession_options_show = context.app.zero_deposit_eligibility_page.does_profession_options_show()
    assert does_profession_options_show == True, "Working Individuals, Student and Business Owners options are not " \
                                                 "showing."


@step('Upload files section for foreign citizen is shown')
def step_impl(context):
    does_all_related_fields_show = context.app.zero_deposit_eligibility_page.does_upload_section_for_foreigner_show()
    assert does_all_related_fields_show == True, "Upload Files section for Foreigner is not showing"
