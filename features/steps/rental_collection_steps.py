import time
from features.credentials import *
from behave import given, when, then, step


@step('Rental Collection details are shown')
def step_impl(context):
    does_rc_details_show = context.app.rental_collection.does_rc_details_page_show()
    assert does_rc_details_show == True, "Rental collection details are not showing"


@step('A rental collection record is clicked')
def step_impl(context):
    context.app.rental_collection.click_on_record()



