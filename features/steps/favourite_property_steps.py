import time
from features.credentials import *
from behave import given, when, then, step


@step('A property is added as favourite')
def step_impl(context):
    is_added_to_favourite = context.app.favourite_page.add_to_favourite()
    assert is_added_to_favourite == True, "Liked This Property toast message did not show"


@step('A property is removed from favourite')
def step_impl(context):
    is_removed_from_favourite = context.app.favourite_page.remove_from_favourites()
    assert is_removed_from_favourite == True, "Property removed from favourites toast message did not show"


@step('Added favourite property shows under favourite listing')
def step_impl(context):
    is_property_showing = context.app.favourite_page.verify_property_is_add_as_fav()
    assert is_property_showing == True, "Liked property is not showing under Favourite Listings"


@step('Removed favourite property does not show under favourite listing')
def step_impl(context):
    is_property_showing = context.app.favourite_page.verify_removed_from_favourites()
    assert is_property_showing == True, "Liked property is not showing under Favourite Listings"


@step('A property card is clicked')
def step_impl(context):
    context.app.favourite_page.click_first_property()




