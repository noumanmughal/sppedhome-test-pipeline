import time
from features.credentials import *
from behave import given, when, then, step


@step('Home screen is shown after successful login')
def step_impl(context):
    does_home_page_show = context.app.home_page.does_home_page_show()
    assert does_home_page_show == True, "Home screen is not showing after successful login"


@step('More button is clicked')
def step_impl(context):
    context.app.home_page.click_on_more_btn()


@step('Properties button is clicked')
def step_impl(context):
    context.app.home_page.click_on_properties_btn()


@step('Home button is clicked')
def step_impl(context):
    context.app.home_page.click_on_home_btn()





