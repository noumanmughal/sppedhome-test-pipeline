import time
from features.credentials import *
from behave import given, when, then, step


@step('Edit profile button is clicked')
def step_impl(context):
    context.app.more_page.click_on_edit_profile_btn()


@step('Rental Collection button is clicked')
def step_impl(context):
    context.app.more_page.click_rental_collection_btn()


@step('More page with the related options show')
def step_impl(context):
    does_more_page_with_options_show = context.app.more_page.does_more_page_options_show()
    assert does_more_page_with_options_show == True, "More page is not showing"


@given("User is on More Page after login with valid otp")
def step_impl(context):
    context.execute_steps(u"""
    Given Enter your mobile number screen is shown
    And Valid registered mobile number is entered
    And Next button is clicked
    When Request 4 digit OTP button is clicked
    And Valid otp code is entered
    Then Home screen is shown after successful login
    And More button is clicked
    """)


@given("User is signed in with valid otp")
def step_impl(context):
    context.execute_steps(u"""
    Given Enter your mobile number screen is shown
    And Valid registered mobile number is entered
    And Next button is clicked
    When Request 4 digit OTP button is clicked
    And Valid otp code is entered
    Then Home screen is shown after successful login
    """)





