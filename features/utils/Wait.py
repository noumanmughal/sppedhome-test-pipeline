from threading import Thread
import time


class Wait:
    def __init__(self, driver):
        self.driver = driver

    def wait1s(self):
        time.sleep(1)

    def wait2s(self):
        time.sleep(2)

    def wait3s(self):
        time.sleep(3)

    def wait4s(self):
        time.sleep(4)

    def wait5s(self):
        time.sleep(5)

    def waitFor(self, seconds):
        time.sleep(55)
