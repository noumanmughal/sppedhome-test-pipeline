from features.pages.base_page import Page
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait


class SearchProperty(Page):
    SORT_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='keywordSearchListSortIconBtn']")
    FILTER_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='keywordSearchListFilterBtn']")
    MAP_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='keywordSearchListMapBtn']")
    TOTAL_RESULTS_TXT = (By.XPATH, "//android.widget.TextView[contains(@text, 'results')]")
    PROPERTY_CARD = (By.XPATH, "(//android.view.ViewGroup[@content-desc='propCardSliderBtn'])")

    BEST_MATCH_ON_TXT = (By.XPATH, "//android.widget.TextView[contains(@text, 'Best Match is on')]")
    BEST_MATCH_ON_TOGGLE = (By.XPATH, "(//android.view.ViewGroup[2]/android.view.ViewGroup)[7]")

    SEARCH_BAR = (By.XPATH, "//android.widget.EditText[@content-desc='editTextHomeSearchPropertyTextBox']")
    SEARCH_TXT_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='homeSearchPropertyTextBox']")
    SEARCH_ICON = (By.XPATH, "//android.view.ViewGroup[@content-desc='editTextHomeSearchIconBtn']")

    RELEVANCE_SORT = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListCountriesBtn'])[1]")
    DISTANCE_SORT = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListCountriesBtn'])[2]")
    PRICE_SORT = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListCountriesBtn'])[3]")
    TICK_RELEVANCE = (
        By.XPATH,
        "(//android.view.ViewGroup[@content-desc='keywordSearchListCountriesBtn'])[1]/android.view.ViewGroup[1]")
    TICK_DISTANCE = (
        By.XPATH,
        "(//android.view.ViewGroup[@content-desc='keywordSearchListCountriesBtn'])[2]/android.view.ViewGroup[1]")
    TICK_PRICE = (
        By.XPATH,
        "(//android.view.ViewGroup[@content-desc='keywordSearchListCountriesBtn'])[3]/android.view.ViewGroup[1]")
    SORT_NOW_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='keywordSearchListSortNowbtn']")
    SORT_POP_UP_CLOSE_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='keywordSearchListModalBtn']")

    RECENT_SEARCHES_TXT = (By.XPATH, "//android.widget.TextView[contains(@text, 'Recent searches')]")
    RECENT_SEARCHES_LIST = (By.XPATH, "(//android.view.ViewGroup[@content-desc='homeRecentListBtn'])")

    BACK_BTN_SEARCH_RESULTS = (By.XPATH, "//android.view.ViewGroup[@content-desc='keyboardLeftBtn']")

    def is_sort_btn_present(self):
        result = self.is_element_present(self.SORT_BTN)
        return result

    def is_filter_btn_present(self):
        result = self.is_element_present(self.FILTER_BTN)
        return result

    def is_map_btn_present(self):
        result = self.is_element_present(self.MAP_BTN)
        return result

    def does_search_return_results(self):
        self.turn_off_best_match()
        Wait.wait5s(self)

        is_total_zero = True
        does_property_cards_show = False

        total_results_element = self.find_element(self.TOTAL_RESULTS_TXT)
        total_txt = total_results_element.text
        total_number = int(total_txt.split(' ')[1])

        if total_number != 0:
            is_total_zero = False

        property_card = self.find_elements(self.PROPERTY_CARD)
        if len(property_card) > 0:
            does_property_cards_show = True

        if does_property_cards_show and not is_total_zero:
            return True
        else:
            return False

    def is_best_match_txt_present(self):
        result = self.is_element_present(self.BEST_MATCH_ON_TXT)
        return result

    def turn_off_best_match(self):
        if self.is_best_match_txt_present():
            self.click_on_element(self.BEST_MATCH_ON_TOGGLE)
            Wait.wait2s(self)

    def click_search_icon(self):
        self.click_on_element(self.SEARCH_ICON)
        Wait.wait2s(self)

    def open_search_bar(self):
        self.click_on_element(self.SEARCH_BAR)
        Wait.wait5s(self)

    def property_search(self, address):
        self.open_search_bar()
        self.input(address, self.SEARCH_TXT_FIELD)
        Wait.wait3s(self)

        self.driver.press_keycode(66)
        Wait.wait3s(self)

    def does_search_options_show(self):
        Wait.wait5s(self)
        if self.is_sort_btn_present() and \
                self.is_filter_btn_present() and \
                self.is_map_btn_present():
            return True
        else:
            return False

    def click_sort_filter_btn(self):
        self.click_on_element(self.SORT_BTN)

    def click_filters_btn(self):
        self.click_on_element(self.FILTER_BTN)

    def click_map_btn(self):
        self.click_on_element(self.MAP_BTN)
        Wait.wait5s(self)

    def is_relevance_sort_btn_present(self):
        result = self.is_element_present(self.RELEVANCE_SORT)
        return result

    def is_distance_sort_btn_present(self):
        result = self.is_element_present(self.DISTANCE_SORT)
        return result

    def is_price_sort_btn_present(self):
        result = self.is_element_present(self.PRICE_SORT)
        return result

    def is_sort_now_btn_present(self):
        result = self.is_element_present(self.SORT_NOW_BTN)
        return result

    def does_sort_pop_up_with_options_show(self):
        if self.is_relevance_sort_btn_present() and \
                self.is_distance_sort_btn_present() and \
                self.is_price_sort_btn_present() and \
                self.is_sort_now_btn_present():
            return True
        else:
            return False

    def does_recent_searches_show(self):
        elements = self.find_elements(self.RECENT_SEARCHES_LIST)
        total_elements = len(elements)
        if total_elements > 0 and self.find_element(self.RECENT_SEARCHES_TXT):
            return True
        else:
            return False

    def click_back_btn_on_search_results(self):
        self.click_on_element(self.BACK_BTN_SEARCH_RESULTS)
