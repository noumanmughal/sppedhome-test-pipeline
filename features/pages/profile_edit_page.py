import base64
import os
import random

from features.pages.base_page import Page
from features.pages.more_page import MorePage
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait
from appium.webdriver.common.touch_action import TouchAction
from PIL import Image, ImageChops


class ProfileEditPage(MorePage):
    BACK_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='editPersonBackBtn']")
    EDIT_PERSONAL_INFO_SCREEN_TITLE = (By.XPATH, "//android.widget.TextView[@text = 'Edit personal info']")
    PROFILE_IMG = (
        By.XPATH, "//android.view.ViewGroup[@content-desc='editPersonProfileImageBtn']/android.widget.ImageView")
    PERSONAL_DETAILS_TITLE = (By.XPATH, "//android.widget.TextView[@text = 'Personal detail']")
    NAME_TXT_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='editPerInfoNameInput']")
    EMAIL_TXT_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='editPerInfoEmailInput']")
    PHONE_NUMBER_TXT_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='editPerInfoPhoneInput']")
    WHATSAPP_NUMBER_TXT_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='editPerInfoWPNumberInput']")
    DOB_DROPDOWN = (By.XPATH, "//android.widget.EditText[@content-desc='editPerInfoSelectDateInput']")
    GENDER_DROPDOWN = (By.XPATH, "//android.widget.EditText[@content-desc='editPerInfoGenderInput']")
    LOCATION_TXT_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='editPerInfoLocationInput']")
    NATIONALITY_DROPDOWN = (By.XPATH, "//android.view.ViewGroup[@content-desc='editPersonCountryBtn']")
    CHANGE_PASSWORD_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Change Password']")
    UPDATE_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='editPersonUpdateBtn']")
    ADDITIONAL_INFO_TXT = (By.XPATH, "//android.widget.TextView[@text = 'Additional info']")
    ADDITIONAL_INFO_PROPERTIES_DROPDOWN = (
        By.XPATH, "//android.widget.EditText[@content-desc='editPerInfoNumbPropInput']")
    ADDITIONAL_INFO_EXPERTISE_DROPDOWN = (By.XPATH, "//android.widget.EditText[@content-desc='editPerInfoExpInput']")

    EDIT_PROFILE_LANDLORD_TAB_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='editPersonLandloardBtn']")
    EDIT_PROFILE_TENANT_TAB_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='editPersonTenantBtn']")

    EDIT_PROFILE_IMG = (
    By.XPATH, "//android.view.ViewGroup[@content-desc='editPersonProfileImageBtn']/android.widget.ImageView")
    CAMERA_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='editPersonCameraImageBtn']")
    GALLERY_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Gallery']")
    RECENT_IMAGES_BTN = (By.XPATH, "(//com.google.android.material.chip.Chip)[1]")
    THREE_LINES_BTN = (By.XPATH, "//android.widget.ImageButton")
    ALL_IMAGES = (By.XPATH, "//android.view.View")
    DOWNLOADS_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Downloads']")

    ALERT_POP_UP_OK_BTN = (By.XPATH, "//android.widget.Button")
    ALERT_POP_UP_UPDATE_SUCCESS_TXT = (By.XPATH, "//android.widget.TextView[@text = 'Profile updated successfully.']")

    MORE_SCREEN_PP = (By.XPATH, "(//android.widget.ImageView)[1]")

    PROFILE_PICTURE_BEFORE_UPDATE = ''
    PROFILE_PICTURE_AFTER_UPDATE = ''
    PROFILE_NAME = ''
    PROFILE_EMAIL = ''

    def is_edit_profile_title_present(self):
        result = self.is_element_present(self.EDIT_PERSONAL_INFO_SCREEN_TITLE)
        return result

    def is_profile_image_present(self):
        result = self.is_element_present(self.PROFILE_IMG)
        return result

    def is_personal_details_title_present(self):
        result = self.is_element_present(self.PERSONAL_DETAILS_TITLE)
        return result

    def is_dob_dropdown_present(self):
        result = self.is_element_present(self.DOB_DROPDOWN)
        return result

    def is_gender_dropdown_present(self):
        result = self.is_element_present(self.GENDER_DROPDOWN)
        return result

    def is_nationality_dropdown_present(self):
        result = self.is_element_present(self.NATIONALITY_DROPDOWN)
        return result

    def is_additional_info_title_present(self):
        result = self.is_element_present(self.ADDITIONAL_INFO_TXT)
        return result

    def is_ai_properties_dropdown_present(self):
        result = self.is_element_present(self.ADDITIONAL_INFO_PROPERTIES_DROPDOWN)
        return result

    def is_ai_expertise_dropdown_present(self):
        result = self.is_element_present(self.ADDITIONAL_INFO_EXPERTISE_DROPDOWN)
        return result

    def is_change_password_btn_present(self):
        result = self.is_element_present(self.CHANGE_PASSWORD_BTN)
        return result

    def is_name_present(self):
        element = self.find_element(self.NAME_TXT_FIELD)
        if len(element.text) > 0:
            return True
        else:
            return False

    def is_email_present(self):
        element = self.find_element(self.EMAIL_TXT_FIELD)
        if len(element.text) > 0:
            return True
        else:
            return False

    def is_location_present(self):
        element = self.find_element(self.LOCATION_TXT_FIELD)
        if len(element.text) > 0:
            return True
        else:
            return False

    def is_phone_number_present(self):
        element = self.find_element(self.PHONE_NUMBER_TXT_FIELD)
        if len(element.text) > 0:
            return True
        else:
            return False

    def is_whatsapp_number_present(self):
        element = self.find_element(self.WHATSAPP_NUMBER_TXT_FIELD)
        if len(element.text) > 0:
            return True
        else:
            return False

    def is_update_btn_present(self):
        result = self.is_element_present(self.UPDATE_BTN)
        return result

    def does_edit_profile_page_show(self):
        Wait.wait5s(self)
        if self.is_edit_profile_title_present() and \
                self.is_name_present() and \
                self.is_phone_number_present() and \
                self.is_profile_image_present():
            return True
        else:
            return False

    def are_top_elements_present(self):
        if self.is_name_present() and \
                self.is_email_present() and \
                self.is_whatsapp_number_present() and \
                self.is_phone_number_present() and \
                self.is_profile_image_present():
            self.swipe_up(1)
            return True
        else:
            return False

    def are_mid_elements_present(self):
        if self.is_dob_dropdown_present() and \
                self.is_gender_dropdown_present() and \
                self.is_location_present() and \
                self.is_nationality_dropdown_present() and \
                self.is_change_password_btn_present():
            return True
        else:
            return False

    def are_bottom_elements_present(self):
        self.click_landlord_btn()
        self.swipe_up(1)
        if self.is_ai_expertise_dropdown_present() and \
                self.is_ai_properties_dropdown_present() and \
                self.is_additional_info_title_present() and \
                self.is_update_btn_present():
            self.swipe_up(1)
            return True
        else:
            return False

    def does_edit_profile_page_with_all_info_show(self):
        Wait.wait5s(self)
        if self.are_top_elements_present() and \
                self.are_mid_elements_present() and \
                self.are_bottom_elements_present():
            return True
        else:
            return False

    def click_update_btn(self):
        self.swipe_up(2)
        self.click_on_element(self.UPDATE_BTN)
        Wait.wait2s(self)

    def click_alert_popup_ok_btn(self):
        self.click_on_element(self.ALERT_POP_UP_OK_BTN)
        Wait.wait2s(self)

    def click_edit_profile_back_btn(self):
        self.click_on_element(self.BACK_BTN)
        Wait.wait5s(self)

    def save_profile_picture_screenshot(self, name):
        directory = os.getcwd() + "\\resources\\"
        file_name = f"{name}.png"
        save_at = directory + file_name
        element = self.find_element(self.MORE_SCREEN_PP)
        element.screenshot(save_at)
        return save_at

    def click_landlord_btn(self):
        self.click_on_element(self.EDIT_PROFILE_LANDLORD_TAB_BTN)
        Wait.wait2s(self)

    def are_screenshots_identical(self, image1, image2):
        first = Image.open(image1)
        second = Image.open(image2)
        diff = ImageChops.difference(first, second)

        if list(first.getdata()) == list(second.getdata()):
            #print("Identical")
            return True
        else:
            #print("Different")
            return False

    def change_profile_pic(self):
        self.send_profile_pictures_to_emulator()
        Wait.wait3s(self)

        self.PROFILE_PICTURE_BEFORE_UPDATE = self.save_profile_picture_screenshot('pp_before')

        self.click_on_edit_profile_btn()

        self.click_on_element(self.CAMERA_BTN)
        Wait.wait2s(self)

        self.click_on_element(self.GALLERY_BTN)
        Wait.wait2s(self)

        self.click_on_element(self.THREE_LINES_BTN)
        Wait.wait2s(self)

        self.click_on_downloads_btn()

        self.click_on_element(self.RECENT_IMAGES_BTN)

        self.swipe_up(1)

        all_images = self.find_elements(self.ALL_IMAGES)
        index = random.randint(1, len(all_images) - 2)
        all_images[index].click()
        Wait.wait2s(self)

        self.click_update_btn()

        self.click_alert_popup_ok_btn()

        self.click_edit_profile_back_btn()

        self.PROFILE_PICTURE_AFTER_UPDATE = self.save_profile_picture_screenshot("pp_after")

    def is_profile_picture_updated(self):
        check_count = 0
        result = self.are_screenshots_identical(self.PROFILE_PICTURE_BEFORE_UPDATE, self.PROFILE_PICTURE_AFTER_UPDATE)

        if not result:
            return True

        elif result and check_count == 0:
            is_found_in_second_try = self.update_profile_picture_again()
            return is_found_in_second_try

    def update_profile_picture_again(self):
        self.change_profile_pic()
        if self.are_screenshots_identical(self.PROFILE_PICTURE_BEFORE_UPDATE, self.PROFILE_PICTURE_AFTER_UPDATE):
            return False
        else:
            return True

    def remove_saved_screenshots(self):
        os.remove(self.PROFILE_PICTURE_BEFORE_UPDATE)
        os.remove(self.PROFILE_PICTURE_AFTER_UPDATE)

    def set_current_name(self):
        txt = self.get_element_txt(self.NAME_TXT_FIELD)
        self.PROFILE_NAME = txt

    def set_current_email(self):
        txt = self.get_element_txt(self.EMAIL_TXT_FIELD)
        self.PROFILE_EMAIL = txt

    def change_profile_details(self):
        self.set_current_name()
        self.set_current_email()
        name_to_be = None

        if ' ' in self.PROFILE_NAME:
            name_to_be = f"{self.PROFILE_NAME[:self.PROFILE_NAME.index(' ')]} {self.random_string(5)}"
        else:
            name_to_be = f"{self.PROFILE_NAME} {self.random_string(5)}"
        email_to_be = f"{self.random_string(5).lower()}@test.com"

        self.input(name_to_be.title(), self.NAME_TXT_FIELD)
        self.input(email_to_be, self.EMAIL_TXT_FIELD)

        self.click_update_btn()

    def does_success_notification_show(self):
        result = self.is_element_present(self.ALERT_POP_UP_UPDATE_SUCCESS_TXT)
        return result

    def is_profile_details_updated(self):
        fields_updated = False
        does_update_notification_show = self.does_success_notification_show()
        self.click_alert_popup_ok_btn()

        self.swipe_down(2)

        before_name = self.PROFILE_NAME
        before_email = self.PROFILE_EMAIL

        self.set_current_name()
        self.set_current_email()

        after_name = self.PROFILE_NAME
        after_email = self.PROFILE_EMAIL

        if before_name != after_name and before_email != after_email:
            fields_updated = True
        else:
            fields_updated = False

        if does_update_notification_show and fields_updated:
            return True
        else:
            return False

    def click_on_downloads_btn(self):
        elements = self.find_elements(self.DOWNLOADS_BTN)
        total_elements = len(elements)
        elements[total_elements - 1].click()
        # if total_elements == 3:
        #     elements[2].click()
        # elif total_elements == 2:
        #     elements[1].click()
        # else:
        #     elements[0].click()

