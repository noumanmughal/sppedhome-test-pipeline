from features.pages.base_page import Page
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait
from features.pages.favourite_page import FavouritePage


class ChatButtonPage(FavouritePage):
    BOOK_VIRTUAL_APPOINTMENT_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Book Virtual Appointment']")
    CRM_TITLE = (By.XPATH, "//android.widget.TextView[@text = 'Chat Request']")
    MOVE_IN_DATE_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='chatReqScreenMyDateInput']")
    MAKE_OFFER_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='chatReqScreenBudgetInput']")
    TENANCY_DURATION_FIELD = (By.XPATH, "//android.view.ViewGroup[@content-desc='chatRefTenancyBtn']")
    BE_INFORMED_CHECKBOX = (By.XPATH, "//android.view.ViewGroup[@content-desc='chatRefCheckBoxBtn']")
    CHAT_REQUEST_SUBMIT_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='chatRefSubmitBtn']")

    OPEN_CHAT_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Open Chat']")

    MONTHS_12_TENANCY_DURATION = (By.XPATH, "//android.widget.TextView[@text = '12 Months']")
    MOVE_IN_DATE_CONFIRM_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='moveInConfirmBtn']")
    MOVE_IN_DATE = (By.XPATH, "//android.widget.TextView[@text = '22']")

    VIEWING_TIME_TITLE = (By.XPATH, "//android.widget.TextView[@text = 'Viewing Time']")
    VIEWING_DATE_SELECTOR = (By.XPATH, "//android.view.ViewGroup[@content-desc='instantModalSelectedDateBtn']")
    VIEWING_TIME_BOXES = (By.XPATH, "(//android.view.ViewGroup[@content-desc='instantModalListBtn'])")
    VIEWING_TIME_SUBMIT_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='instantModalSubmitBtn']")

    ADDITIONAL_QUESTIONS_TITLE = (By.XPATH, "//android.widget.TextView[@text = 'Some additional questions']")
    AI_MODAL_NAME_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='chatAddInfoModalCnameInput']")
    AI_MODAL_CONTRACT_DD = (By.XPATH, "//android.widget.EditText[@content-desc='chatAddInfoModalContactInput']")
    AI_MODAL_INCOME_DD = (By.XPATH, "//android.widget.EditText[@content-desc='chatAddInfoModalIncomeInput']")
    AI_MODAL_GENDER_DD = (By.XPATH, "//android.widget.EditText[@content-desc='chatAddInfoModalGenderInput']")
    AI_MODAL_DOB_DD = (By.XPATH, "//android.widget.EditText[@content-desc='chatAddInfoModalMyDateInput']")
    AI_MODAL_SAVE_EXIT_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='addInfoSaveBtn']")

    UP_TO_DATE_CLOSE_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='homeRightChevronBtn']")
    VIEWING_TIME_DATEPICKER_OK_BTN = (By.XPATH, "//android.widget.Button[2]")
    VIEWING_TIME_TIME_PICKET_1PM = (By.XPATH, "(//android.view.ViewGroup[@content-desc='instantModalListBtn'])[2]")

    def is_crm_title_present(self):
        result = self.is_element_present(self.CRM_TITLE)
        return result

    def is_move_in_date_field_present(self):
        result = self.is_element_present(self.MOVE_IN_DATE_FIELD)
        return result

    def is_make_offer_field_present(self):
        result = self.is_element_present(self.MAKE_OFFER_FIELD)
        return result

    def is_tenancy_duration_field_present(self):
        result = self.is_element_present(self.TENANCY_DURATION_FIELD)
        return result

    def is_submit_btn_present(self):
        result = self.is_element_present(self.CHAT_REQUEST_SUBMIT_BTN)
        return result

    def is_open_chat_btn_present(self):
        result = self.is_element_present(self.OPEN_CHAT_BTN)
        return result

    def is_book_appointment_btn_present(self):
        result = self.is_element_present(self.BOOK_VIRTUAL_APPOINTMENT_BTN)
        return result

    def click_book_virtual_appointment_btn(self):
        self.close_upto_date_popup()
        self.swipe_up(1)
        Wait.wait1s(self)
        swipes = 10
        index = 0
        element_found = False
        while not element_found:
            property_cards = self.find_elements(self.HP_PROPERTY_CARDS)
            properties_found = len(property_cards)
            for count in range(properties_found):
                property_cards[count].click()
                #Wait.waitFor(self, 30)

                if self.is_open_chat_btn_present():
                    self.click_on_element(self.GO_BACK_BTN)
                    Wait.wait2s(self)

                elif self.is_book_appointment_btn_present():
                    index = count
                    element_found = True
                    self.click_on_element(self.BOOK_VIRTUAL_APPOINTMENT_BTN)
                    break
                else:
                    self.click_on_element(self.GO_BACK_BTN)
                    Wait.wait2s(self)

            if element_found:
                break
            else:
                #swipes = swipes - 1
                self.swipe_up(2)
                Wait.wait1s(self)

    def does_chat_request_modal_shows(self):
        if self.is_crm_title_present() and \
                self.is_move_in_date_field_present() and \
                self.is_make_offer_field_present() and \
                self.is_tenancy_duration_field_present() and \
                self.is_submit_btn_present():
            return True
        else:
            return False

    def fill_chat_request_modal(self):
        self.click_on_element(self.MOVE_IN_DATE_FIELD)
        Wait.wait2s(self)

        self.swipe_up(1)

        move_in_date_elements = self.find_elements(self.MOVE_IN_DATE)
        move_in_date_elements[0].click()
        Wait.wait1s(self)

        self.click_on_element(self.MOVE_IN_DATE_CONFIRM_BTN)
        Wait.wait1s(self)

        self.input(3000, self.MAKE_OFFER_FIELD)
        Wait.wait1s(self)

        self.click_on_element(self.TENANCY_DURATION_FIELD)
        Wait.wait1s(self)

        self.click_on_element(self.MONTHS_12_TENANCY_DURATION)
        Wait.wait2s(self)

        self.click_on_element(self.CHAT_REQUEST_SUBMIT_BTN)
        Wait.wait2s(self)

    def fill_viewing_time_modal(self):
        self.click_on_element(self.VIEWING_DATE_SELECTOR)
        Wait.wait1s(self)
        self.click_on_element(self.VIEWING_TIME_DATEPICKER_OK_BTN)
        Wait.wait2s(self)
        self.click_on_element(self.VIEWING_TIME_TIME_PICKET_1PM)
        Wait.wait2s(self)

    def click_viewing_time_submit_button(self):
        self.fill_chat_request_modal()
        self.fill_viewing_time_modal()
        self.swipe_up(1)
        self.click_on_element(self.VIEWING_TIME_SUBMIT_BTN)
        Wait.wait2s(self)

    def is_ai_modal_title_present(self):
        result = self.is_element_present(self.ADDITIONAL_QUESTIONS_TITLE)
        return result

    def is_ai_modal_name_present(self):
        result = self.is_element_present(self.AI_MODAL_NAME_FIELD)
        return result

    def is_ai_contract_type_present(self):
        result = self.is_element_present(self.AI_MODAL_CONTRACT_DD)
        return result

    def is_ai_income_present(self):
        result = self.is_element_present(self.AI_MODAL_INCOME_DD)
        return result

    def is_ai_gender_present(self):
        result = self.is_element_present(self.AI_MODAL_GENDER_DD)
        return result

    def is_ai_dob_present(self):
        result = self.is_element_present(self.AI_MODAL_DOB_DD)
        return result

    def is_ai_save_exit_btn_present(self):
        result = self.is_element_present(self.AI_MODAL_SAVE_EXIT_BTN)
        return result

    def does_additional_info_modal_shows(self):
        if self.is_ai_modal_title_present() and \
                self.is_ai_modal_name_present() and \
                self.is_ai_contract_type_present() and \
                self.is_ai_income_present() and \
                self.is_ai_dob_present() and \
                self.is_ai_gender_present() and \
                self.is_ai_save_exit_btn_present():

            return True
        else:
            return False

    def close_upto_date_popup(self):
        self.click_on_element(self.UP_TO_DATE_CLOSE_BTN)
        Wait.wait1s(self)
