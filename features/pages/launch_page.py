from selenium.webdriver.common.by import By

from features.pages.base_page import Page


class LaunchPage(Page):
    ###### Locators

    WELCOME_TO_TEXT = (By.XPATH, "//android.widget.TextView[@text = 'Welcome to']")
    SPEED_TEXT = (By.XPATH, "//android.widget.TextView[@text = 'SPEED']")
    HOME_TEXT = (By.XPATH, "//android.widget.TextView[@text = 'HOME']")
    LOGIN_BTN = (
        By.XPATH,
        "//android.view.ViewGroup[@content-desc='commonCompBtn']/android.view.ViewGroup/android.widget.TextView")
    LOGIN_LATER_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='loginLaterBtn']/android.widget.TextView")

    ###### Locators

    ###### Actions

    def is_login_button_present(self):
        result = self.is_element_present(self.LOGIN_BTN)
        return result

    def is_login_later_button_present(self):
        result = self.is_element_present(self.LOGIN_LATER_BTN)
        return result

    def click_on_login_btn(self):
        self.click_on_element(self.LOGIN_BTN)

    def click_on_login_later_btn(self):
        self.click_on_element(self.LOGIN_LATER_BTN)

    def is_app_name_present(self):
        if self.is_element_present(self.WELCOME_TO_TEXT) and self.is_element_present(
                self.SPEED_TEXT) and self.is_element_present(self.HOME_TEXT):
            return True
        else:
            return False

    ###### Actions
