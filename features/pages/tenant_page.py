from features.pages.base_page import Page
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait
from features.pages.favourite_page import FavouritePage


class TenantView(FavouritePage):
    SHARE_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='disReportVideoBtn']")
    HEART_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='disReportFavBtn']")

    PHOTOS_VIEWER = (By.XPATH, "//android.view.ViewGroup[@content-desc='disReportPrImageBtn']/android.widget.ImageView")
    PHOTOS_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='detailInfoInsPhotoBtn']")
    MAP_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='detailInfoMapBtn']")

    LAST_UPDATE_TXT = (By.XPATH, "//android.widget.TextView[contains(@text, 'Last Update')]")
    OPEN_CHAT_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='chatDetailOpenChatBtn']")
    TO_MOVE_IN_TXT = (By.XPATH, "//android.widget.TextView[contains(@text, 'To Move In')]")
    DESCRIPTION_TXT = (By.XPATH, "//android.widget.TextView[contains(@text, 'Description')]")
    REPORT_LISTING_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='disReportNowBtn']")

    def is_share_btn_present(self):
        result = self.is_element_present(self.SHARE_BTN)
        return result

    def is_heart_btn_present(self):
        result = self.is_element_present(self.HEART_BTN)
        return result

    def is_photo_viewer_present(self):
        self.click_on_element(self.PHOTOS_BTN)
        Wait.wait2s(self)
        result = self.is_element_present(self.PHOTOS_VIEWER)
        return result

    def is_photos_btn_present(self):
        result = self.is_element_present(self.PHOTOS_BTN)
        return result

    def is_map_btn_present(self):
        result = self.is_element_present(self.MAP_BTN)
        return result

    def is_last_update_present(self):
        result = self.is_element_present(self.LAST_UPDATE_TXT)
        return result

    def is_move_in_present(self):
        self.scroll_to_text("To Move In")
        result = self.is_element_present(self.TO_MOVE_IN_TXT)
        return result

    def is_report_btn_present(self):
        result = self.is_element_present(self.REPORT_LISTING_BTN)
        return result

    def is_description_present(self):
        result = self.is_element_present(self.DESCRIPTION_TXT)
        return result

    def is_chat_btn_present(self):
        result = self.is_element_present(self.OPEN_CHAT_BTN)
        return result

    def are_top_elements_present(self):
        if self.is_share_btn_present() and \
                self.is_heart_btn_present() and \
                self.is_photo_viewer_present() and \
                self.is_map_btn_present() and \
                self.is_photos_btn_present():
            return True
        else:
            return False

    def are_mid_elements_present(self):
        self.swipe_up(1)
        if self.is_last_update_present() and \
                self.is_description_present():
            return True
        else:
            return False

    def are_bottom_elements_present(self):
        self.swipe_up(2)
        if self.is_report_btn_present():
            return True
        else:
            return False

    def does_property_details_show(self):
        if self.are_top_elements_present() and \
                self.are_mid_elements_present() and \
                self.are_bottom_elements_present():
            return True
        else:
            return False


