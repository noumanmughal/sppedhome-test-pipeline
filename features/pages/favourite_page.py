from features.pages.base_page import Page
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait
from features.pages.home_page import HomePage


class FavouritePage(HomePage):
    HP_FIRST_PROPERTY_NAME = ""
    HP_FIRST_PROPERTY_TYPE = ""
    HP_FIRST_PROPERTY_PRICE = ""
    FAV_PROPERTY_COUNT_BEFORE = ""

    GO_BACK_BTN = (By.XPATH,
                   "//android.view.ViewGroup[@content-desc='disReportBackBtn']/android.view.ViewGroup/android.widget.TextView")
    FAV_TAB_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='myListingContFavBtn']")
    FAV_LISTINGS_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='myFavListingBtn']")
    FAV_LISTING_CARDS = (By.XPATH, "//android.view.ViewGroup[@content-desc='myFavDetailBtn']/android.view.ViewGroup")
    UPCOMING_APPOINTMENTS_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='myFavUpCommingAppBtn']")
    HEART_BTN_FAV_LISTING_CARD = (By.XPATH, "//android.view.ViewGroup[@content-desc='myFavIconBtn']")

    HP_PROPERTY_CARDS = (By.XPATH, "(//android.view.ViewGroup[@content-desc='propCardSliderBtn'])")
    HP_FIRST_PROPERTY_CARD = (By.XPATH, "(//android.view.ViewGroup[@content-desc='propCardSliderBtn'])[1]")
    FAV_BTN_PROPERTY_DETAILS = (By.XPATH, "//android.view.ViewGroup[@content-desc='disReportFavBtn']")
    LIKED_THIS_PROPERTY_TXT = (By.XPATH, "//android.widget.TextView[@text = 'Liked this property']")
    REMOVED_FROM_FAVOURITES_TXT = (By.XPATH, "//android.widget.TextView[@text = 'Property removed from favourites']")

    def go_to_fav_property_screen(self):
        self.click_on_properties_btn()
        Wait.wait2s(self)
        self.click_on_element(self.FAV_TAB_BTN)
        Wait.wait2s(self)
        self.click_on_element(self.UPCOMING_APPOINTMENTS_BTN)
        Wait.wait2s(self)
        self.click_on_element(self.FAV_LISTINGS_BTN)
        Wait.wait2s(self)

    def set_fav_property_count_before(self):
        self.go_to_fav_property_screen()
        total_cards_element = self.find_elements(self.FAV_LISTING_CARDS)
        total_cards_before = len(total_cards_element)
        print(f"Fav count before = {total_cards_before}")
        self.FAV_PROPERTY_COUNT_BEFORE = total_cards_before

        self.click_on_home_btn()

    def click_hp_first_property(self):
        self.go_to_fav_property_screen()
        self.remove_all_favourites()
        self.click_on_home_btn()

        self.swipe_up(1)
        Wait.wait2s(self)

        first_card = self.find_element(self.HP_FIRST_PROPERTY_CARD)

        name_xpath = "//android.widget.TextView[3]"
        type_xpath = "//android.widget.TextView[2]"
        price_xpath = "//android.widget.TextView[1]"

        self.HP_FIRST_PROPERTY_NAME = first_card.find_element_by_xpath(name_xpath).text
        self.HP_FIRST_PROPERTY_TYPE = first_card.find_element_by_xpath(type_xpath).text
        self.HP_FIRST_PROPERTY_PRICE = first_card.find_element_by_xpath(price_xpath).text

        self.click_on_element(self.HP_FIRST_PROPERTY_CARD)
        Wait.wait5s(self)

    def add_to_favourite(self):
        self.click_hp_first_property()
        self.click_on_element(self.FAV_BTN_PROPERTY_DETAILS)
        result = self.is_element_present(self.LIKED_THIS_PROPERTY_TXT)
        self.click_on_element(self.GO_BACK_BTN)
        return result

    def verify_property_is_add_as_fav(self):
        self.go_to_fav_property_screen()

        total_cards_element = self.find_elements(self.FAV_LISTING_CARDS)
        total_cards_after = len(total_cards_element)
        print(f"FAv Cards After : {total_cards_after}")

        if total_cards_after != self.FAV_PROPERTY_COUNT_BEFORE:
            return True
        else:
            return False

    def remove_from_favourites(self):
        self.set_fav_property_count_before()
        self.go_to_fav_property_screen()
        self.click_on_element(self.HEART_BTN_FAV_LISTING_CARD)
        result = self.is_element_present(self.REMOVED_FROM_FAVOURITES_TXT)
        return result

    def remove_all_favourites(self):
        element_present = True
        while element_present:
            total_cards_element = self.find_elements(self.FAV_LISTING_CARDS)
            total_found = len(total_cards_element)
            if total_found > 0:
                self.click_on_element(self.HEART_BTN_FAV_LISTING_CARD)
                total_found -= 1
                Wait.wait2s(self)
            else:
                element_present = False
                self.FAV_PROPERTY_COUNT_BEFORE = 0
                break

    def verify_removed_from_favourites(self):
        self.go_to_fav_property_screen()
        total_cards_element = self.find_elements(self.FAV_LISTING_CARDS)
        total_cards_after_removing = len(total_cards_element)

        if self.FAV_PROPERTY_COUNT_BEFORE != total_cards_after_removing:
            return True
        else:
            return False

    def click_first_property(self):
        self.swipe_up(1)
        self.click_on_element(self.HP_FIRST_PROPERTY_CARD)
        Wait.wait5s(self)

    # def find_property_with_appointment_btn(self):
    #     self.swipe_up(1)
    #     swipes = 3
    #     index = 0
    #     while swipes != 0:
    #         property_cards = self.find_elements(self.HP_PROPERTY_CARDS)
    #         properties_found = len(property_cards)
    #         print(f"PROPERTIES FOUND : {properties_found}")
    #         for count in range(properties_found):
    #             property_cards[count].click()
    #             result = self.is_element_present(self.BVA)
    #             if result:
    #                 index = count
    #                 print(f"Found BVA at index : {count}")
    #                 break
    #             else:
    #                 self.click_on_element(self.GO_BACK_BTN)
    #                 Wait.wait2s(self)
    #
    #         if index > 0:
    #             break
    #         else:
    #             swipes = swipes - 1
    #             self.swipe_up(1)

