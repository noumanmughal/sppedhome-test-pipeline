from features.pages.base_page import Page
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait


class ZeroDepositEligibilityPage(Page):
    ZDE_PAGE_TITLE_TXT = (By.XPATH, "//android.widget.TextView[@text = 'Zero Deposit Eligibility']")
    CREDIT_CHECK_TXT = (By.XPATH, "//android.widget.TextView[@text = 'Credit Check']")

    MALAYSIAN_CITIZEN_BTN = (By.XPATH, "//android.widget.TextView[@text = 'I am a Malaysian Citizen']")
    FOREIGNER_BTN = (By.XPATH, "//android.widget.TextView[@text = 'I am a Foreigner']")

    UPLOAD_NRIC_FRONT = (By.XPATH, "(//android.view.ViewGroup[1])[15]")
    UPLOAD_NRIC_BACK = (By.XPATH, "(//android.view.ViewGroup[2])[2]")
    NAME_TXT_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='zeroDepNricName']")
    NRIC_NUMBER_TXT_FIELD = (By.XPATH, "(//android.widget.EditText[@content-desc='zeroDepNricNumber'])[1]")
    EMAIL_TXT_FIELD = (By.XPATH, "(//android.widget.EditText[@content-desc='zeroDepNricNumber'])[2]")
    MOBILE_NUMBER_TXT_FIELD = (By.XPATH, "(//android.widget.EditText[@content-desc='zeroDepNricNumber'])[3]")
    ACKNOWLEDGEMENT_CHECK_BOX = (By.XPATH, "//android.widget.ImageView")
    SUBMIT_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Submit']")

    GET_VERIFIED_SCREEN_TITLE_TXT = (By.XPATH, "//android.widget.TextView[@text = 'Get Verified!']")
    GET_VERIFIED_SCREEN_NEXT_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Next']")
    UPLOAD_IMAGES_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Images']")
    UPLOAD_DOCS_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Docs']")
    SUBMIT_TO_SPEEDHOME_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Submit to SPEEDHOME']")
    WORKING_INDIVIDUALS_BTN = (By.XPATH, "//android.widget.TextView[contains(@text, 'Individuals')]")
    STUDENTS_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Student']")
    BUSINESS_OWNERS_BTN = (By.XPATH, "//android.widget.TextView[contains(@text, 'Business')]")

    NAME_REQUIRED_ERR = (By.XPATH, "//android.widget.TextView[contains(@text, 'be empty')]")
    UPLOAD_FILES_TXT = (By.XPATH, "//android.widget.TextView[@text = 'Upload Files']")
    UPLOAD_FILES_DESC = (By.XPATH, "//android.widget.TextView[contains(@text, 'Please upload pdf, jpg')]")

    def is_malaysian_citizen_btn_present(self):
        result = self.is_element_present(self.MALAYSIAN_CITIZEN_BTN)
        return result

    def is_foreigner_btn_present(self):
        result = self.is_element_present(self.FOREIGNER_BTN)
        return result

    def is_upload_nric_front_btn_present(self):
        result = self.is_element_present(self.UPLOAD_NRIC_FRONT)
        return result

    def is_upload_nric_back_btn_present(self):
        result = self.is_element_present(self.UPLOAD_NRIC_BACK)
        return result

    def is_name_txtfield_present(self):
        result = self.is_element_present(self.NAME_TXT_FIELD)
        return result

    def is_nric_txtfield_present(self):
        result = self.is_element_present(self.NRIC_NUMBER_TXT_FIELD)
        return result

    def is_email_txtfield_present(self):
        result = self.is_element_present(self.EMAIL_TXT_FIELD)
        return result

    def is_mobile_txtfield_present(self):
        self.swipe_up(1)
        result = self.is_element_present(self.MOBILE_NUMBER_TXT_FIELD)
        return result

    def is_submit_btn_present(self):
        self.swipe_up(1)
        result = self.is_element_present(self.SUBMIT_BTN)
        return result

    def click_submit_btn(self):
        self.swipe_up(1)
        self.click_on_element(self.SUBMIT_BTN)

    def does_citizenship_btn_show(self):
        if self.is_malaysian_citizen_btn_present() and self.is_foreigner_btn_present():
            return True
        else:
            return False

    def click_on_malaysian_citizen_btn(self):
        self.click_on_element(self.MALAYSIAN_CITIZEN_BTN)
        Wait.wait3s(self)

    def click_on_foreigner_citizen_btn(self):
        self.click_on_element(self.FOREIGNER_BTN)
        Wait.wait3s(self)

    def click_on_get_verified_next_btn(self):
        self.click_on_element(self.GET_VERIFIED_SCREEN_NEXT_BTN)
        Wait.wait3s(self)

    def is_upload_images_btn_present(self):
        result = self.is_element_present(self.UPLOAD_IMAGES_BTN)
        return result

    def is_upload_docs_btn_present(self):
        result = self.is_element_present(self.UPLOAD_DOCS_BTN)
        return result

    def is_submit_to_speedhome_btn_present(self):
        self.swipe_up(1)
        result = self.is_element_present(self.SUBMIT_TO_SPEEDHOME_BTN)
        return result

    def does_malaysian_citizen_zde_details_show(self):
        if self.is_upload_nric_front_btn_present() and \
                self.is_upload_nric_back_btn_present() and \
                self.is_name_txtfield_present() and \
                self.is_nric_txtfield_present() and \
                self.is_email_txtfield_present() and \
                self.is_mobile_txtfield_present() and \
                self.is_submit_btn_present():
            return True
        else:
            return False

    def is_upload_files_txt_present(self):
        result = self.is_element_present(self.UPLOAD_FILES_TXT)
        return result

    def does_upload_section_for_foreigner_show(self):
        if self.is_upload_files_txt_present() and \
                self.is_upload_images_btn_present() and \
                self.is_upload_docs_btn_present() and \
                self.is_submit_to_speedhome_btn_present():
            return True
        else:
            return False

    def is_name_required_error_present(self):
        result = self.is_element_present(self.NAME_REQUIRED_ERR)
        return result

    def is_acknowledgement_checkbox_present(self):
        self.swipe_up(1)
        result = self.is_element_present(self.ACKNOWLEDGEMENT_CHECK_BOX)
        return result

    def is_working_individual_btn_present(self):
        result = self.is_element_present(self.WORKING_INDIVIDUALS_BTN)
        return result

    def is_student_btn_present(self):
        result = self.is_element_present(self.STUDENTS_BTN)
        return result

    def is_business_owners_btn_present(self):
        result = self.is_element_present(self.BUSINESS_OWNERS_BTN)
        return result

    def does_profession_options_show(self):
        if self.is_working_individual_btn_present() and \
                self.is_student_btn_present() and \
                self.is_business_owners_btn_present():
            return True
        else:
            return False
