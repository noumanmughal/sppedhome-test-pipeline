from features.pages.base_page import Page
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait


class MorePage(Page):
    EDIT_PROFILE_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='editPersonNavBtn']/android.widget.TextView")
    LOGOUT_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='handleLoginBtn']/android.widget.TextView")
    CHECK_NOW_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Check Now']")
    RESUBMIT_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Re-Submit']")

    RENTAL_COLLECTION_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='rentalCollNavBtn']")
    REFER_A_FRIEND_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='referNavBtn']")
    ABOUT_US_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='aboutUsNavBtn']")
    CONTACT_US_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='contactUsNavBtn']")
    WHATSAPP_HELP_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='whatsAppHelpBtn']")
    LANDLORD_HELP_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='landLoardHelpNavBtn']")
    TENANT_HELP_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='tenantHelpNavBtn']")
    VIRTUAL_VIEWING_DROPDOWN = (By.XPATH, "//android.widget.TextView[@text = 'Virtual Viewing']")
    INFO_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='appDetailNavBtn']")
    ABOUT_SPEEDHOME_TXT = (By.XPATH, "//android.widget.TextView[@text = 'ABOUT SPEEDHOME']")

    OK_BTN_LOGOUT_CONFIRMATION_BOX = (By.XPATH, "//android.widget.Button[@text = 'OK']")
    LOGIN_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='handleLoginBtn']/android.widget.TextView")

    def is_edit_profile_btn_present(self):
        result = self.is_element_present(self.EDIT_PROFILE_BTN)
        return result

    def click_on_edit_profile_btn(self):
        self.click_on_element(self.EDIT_PROFILE_BTN)
        Wait.wait2s(self)

    def click_on_logout_btn(self):
        self.click_on_element(self.LOGOUT_BTN)
        Wait.wait2s(self)

    def click_ok_confirmation_box(self):
        self.click_on_element(self.OK_BTN_LOGOUT_CONFIRMATION_BOX)
        Wait.wait2s(self)

    def is_login_btn_present(self):
        result = self.is_element_present(self.LOGIN_BTN)
        return result

    def is_rental_collection_btn_present(self):
        result = self.is_element_present(self.RENTAL_COLLECTION_BTN)
        return result

    def is_refer_friend_btn_present(self):
        result = self.is_element_present(self.REFER_A_FRIEND_BTN)
        return result

    def is_about_speedhome_txt_present(self):
        result = self.is_element_present(self.ABOUT_SPEEDHOME_TXT)
        return result

    def does_more_page_options_show(self):
        if self.is_edit_profile_btn_present() and \
                self.is_rental_collection_btn_present() and \
                self.is_refer_friend_btn_present() and \
                self.is_about_speedhome_txt_present():
            return True
        else:
            return False

    def click_refer_friend_btn(self):
        self.click_on_element(self.REFER_A_FRIEND_BTN)
        Wait.wait2s(self)

    def is_check_now_btn_present(self):
        result = self.is_element_present(self.CHECK_NOW_BTN)
        return result

    def is_re_submit_btn_present(self):
        result = self.is_element_present(self.RESUBMIT_BTN)
        return result

    def is_checknow_or_resubmit_btn_present(self):
        if self.is_re_submit_btn_present() or self.is_check_now_btn_present():
            return True
        else:
            return False

    def click_checknow_or_resubmit_btn(self):
        if self.is_check_now_btn_present():
            self.click_on_element(self.CHECK_NOW_BTN)

        else:
            self.click_on_element(self.RESUBMIT_BTN)

        #Wait.wait3s(self)

    def click_rental_collection_btn(self):
        self.click_on_element(self.RENTAL_COLLECTION_BTN)
        Wait.wait2s(self)
