from features.pages.base_page import Page
from features.pages.search_property_page import SearchProperty
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait


class SearchPropertyFilters(SearchProperty):

    FILTER_PRICE_BAR = (By.XPATH, "(//android.view.ViewGroup[3])[1]")
    HOUSING_TYPE_ROOM_BTN = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListNameBtn'])[1]")
    HOUSING_TYPE_LANDED_BTN = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListNameBtn'])[2]")
    HOUSING_TYPE_HIGH_RISE_BTN = (
        By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListNameBtn'])[3]")
    TITLE_TYPE_FREEHOLD = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListHousingTypeBtn'])[1]")
    TITLE_TYPE_LEASEHOLD = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListHousingTypeBtn'])[2]")
    FURNISH_LEVEL_NO = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListFurnType'])[1]")
    FURNISH_LEVEL_PARTIAL = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListFurnType'])[2]")
    FURNISH_LEVEL_FULL = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListFurnType'])[3]")
    ROOMS_STUDIO = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListBedBtn'])[1]")
    ROOMS_1 = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListBedBtn'])[2]")
    ROOMS_2_PLUS = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListBedBtn'])[3]")
    ROOMS_3_PLUS = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListBedBtn'])[4]")
    ROOMS_4_PLUS = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListBedBtn'])[4]")
    BATHROOMS_1_PLUS = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListBathBtn'])[1]")
    BATHROOMS_2_PLUS = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListBathBtn'])[2]")
    BATHROOMS_3_PLUS = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListBathBtn'])[3]")
    BATHROOMS_4_PLUS = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListBathBtn'])[4]")
    CAR_PARKS_1_PLUS = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListParkBtn'])[1]")
    CAR_PARKS_2_PLUS = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListParkBtn'])[2]")
    CAR_PARKS_3_PLUS = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListParkBtn'])[3]")
    CAR_PARKS_4_PLUS = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListParkBtn'])[4]")
    EXTRA_INFO_LRT_NEARBY = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListExtraInfoBtn'])[1]")
    EXTRA_INFO_PET_FRIENDLY = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListExtraInfoBtn'])[2]")
    EXTRA_INFO_ALL_RACES = (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListExtraInfoBtn'])[3]")
    EXTRA_INFO_INSTANT_VIEW= (By.XPATH, "(//android.view.ViewGroup[@content-desc='keywordSearchListExtraInfoBtn'])[4]")
    RESET_FILTER_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='keywordSearchListResetFilterBtn']")
    FILTER_NOW_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='keywordSearchListFilterNowBtn']")

    LOCATIONS_ON_MAP = (By.XPATH, "//android.view.View[@content-desc='Google Map']/android.view.View")

    def is_filter_pricebar_present(self):
        result = self.is_element_present(self.FILTER_PRICE_BAR)
        return result

    def is_housing_type_filter_btns_present(self):
        if self.is_element_present(self.HOUSING_TYPE_LANDED_BTN) and \
                self.is_element_present(self.HOUSING_TYPE_ROOM_BTN) and \
                self.is_element_present(self.HOUSING_TYPE_HIGH_RISE_BTN):
            return True
        else:
            return False

    def is_title_type_filter_btns_present(self):
        if self.is_element_present(self.TITLE_TYPE_FREEHOLD) and \
                self.is_element_present(self.TITLE_TYPE_LEASEHOLD):
            return True
        else:
            return False

    def is_title_furnish_level_filter_btns_present(self):
        if self.is_element_present(self.FURNISH_LEVEL_NO) and \
                self.is_element_present(self.FURNISH_LEVEL_PARTIAL) and \
                self.is_element_present(self.FURNISH_LEVEL_FULL):
            return True
        else:
            return False

    def is_rooms_filter_btns_present(self):
        if self.is_element_present(self.ROOMS_STUDIO) and \
                self.is_element_present(self.ROOMS_1) and \
                self.is_element_present(self.ROOMS_2_PLUS) and \
                self.is_element_present(self.ROOMS_3_PLUS) and \
                self.is_element_present(self.ROOMS_4_PLUS):
            return True
        else:
            return False

    def is_bathrooms_filter_btns_present(self):
        self.swipe_up(1)
        if self.is_element_present(self.BATHROOMS_1_PLUS) and \
                self.is_element_present(self.BATHROOMS_2_PLUS) and \
                self.is_element_present(self.BATHROOMS_3_PLUS) and \
                self.is_element_present(self.BATHROOMS_4_PLUS):
            return True
        else:
            return False

    def is_car_parks_filter_btns_present(self):
        if self.is_element_present(self.CAR_PARKS_1_PLUS) and \
                self.is_element_present(self.CAR_PARKS_2_PLUS) and \
                self.is_element_present(self.CAR_PARKS_3_PLUS) and \
                self.is_element_present(self.CAR_PARKS_4_PLUS):
            return True
        else:
            return False

    def is_extra_info_filter_btns_present(self):
        if self.is_element_present(self.EXTRA_INFO_LRT_NEARBY) and \
                self.is_element_present(self.EXTRA_INFO_PET_FRIENDLY) and \
                self.is_element_present(self.EXTRA_INFO_ALL_RACES) and \
                self.is_element_present(self.EXTRA_INFO_INSTANT_VIEW):
            return True
        else:
            return False

    def is_filter_now_btn_present(self):
        result = self.is_element_present(self.FILTER_NOW_BTN)
        return result

    def is_reset_filter_btn_present(self):
        result = self.is_element_present(self.RESET_FILTER_BTN)
        return result

    def does_locations_on_map_shows(self):
        elements = self.find_elements(self.LOCATIONS_ON_MAP)
        total_elements = len(elements)
        if total_elements >= 1:
            return True
        else:
            return False
