import glob
import os
import subprocess

from selenium.common.exceptions import NoSuchElementException
from appium.webdriver.common.touch_action import TouchAction
from features.utils.Wait import Wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import string
from random import choice


class Page:
    def __init__(self, driver):
        self.driver = driver

    def find_elements(self, locator):
        return self.driver.find_elements(by=locator[0],
                                         value=locator[1])

    def find_element(self, locator):
        return self.driver.find_element(by=locator[0],
                                        value=locator[1])

    def click_on_element(self, locator):
        element = self.driver.find_element(by=locator[0],
                                           value=locator[1])
        element.click()

    def tap_on_element(self, locator):
        element = self.driver.find_element(by=locator[0],
                                           value=locator[1])
        actions = TouchAction(self.driver)
        actions.tap(element)
        actions.perform()

    def input(self, text, locator):
        input_field = self.driver.find_element(by=locator[0],
                                               value=locator[1])
        input_field.clear()
        input_field.send_keys(text)

    def is_element_present2(self, locator):
        try:
            self.driver.find_element(by=locator[0],
                                     value=locator[1])
        except:
            print("can't find element")
            return False
        return True

    def is_element_txt_present(self, locator):
        element_txt = self.get_element_txt(locator)
        if len(element_txt) > 0:
            return True
        else:
            return False

    def get_element_txt(self, locator):
        element = self.driver.find_element(by=locator[0],
                                           value=locator[1])
        return element.text

    def relaunch_app(self):
        self.driver.background_app(-1)
        Wait.wait2s(self)
        self.driver.launch_app()

    def is_app_installed(self):
        app_installed = self.driver.is_app_installed('com.speedrent')
        return app_installed

    def is_element_present(self, locator):
        try:
            WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(locator))
        # except NoSuchElementException:
        except:
            print(f"can't find element : {locator}")
            return False
        return True

    def is_element_not_present(self, locator):
        try:
            WebDriverWait(self.driver, 60).until(EC.invisibility_of_element_located(locator))
        except:
            return False
        return True

    def random_number(self, length):
        chars = string.digits
        random = ''.join(choice(chars) for _ in range(length))
        return random

    def random_string(self, length):
        chars = string.ascii_letters
        random = ''.join(choice(chars) for _ in range(length))
        return random

    def swipe_up(self, number_of_times):
        size = self.driver.get_window_size()
        # print(size)
        x = size['width']
        y = size['height']
        x1 = x * 0.5
        y1 = y * 0.9
        y2 = x * 0.1
        t = 5000
        # n = 1  # n indicates the number of swipes
        Wait.wait2s(self)
        for i in range(number_of_times):
            self.driver.swipe(x1, y1, x1, y2, t)

    def swipe_down(self, number_of_times):
        size = self.driver.get_window_size()
        # print(size)
        x = size['width']
        y = size['height']
        x1 = x * 0.5
        y1 = y * 0.9
        y2 = x * 0.3
        t = 5000
        # n = 1  # n indicates the number of swipes
        Wait.wait2s(self)
        for i in range(number_of_times):
            self.driver.swipe(x1, y2, x1, y1, t)
            Wait.wait1s(self)

    def send_profile_pictures_to_emulator(self):
        dir_path = os.getcwd() + "\\resources\\images\\profile\\*"
        destination_path = "/sdcard/Download"
        file_paths = glob.glob(dir_path)
        file_names = [os.path.basename(x) for x in file_paths]

        for path in file_paths:
            subprocess.call(f"adb push {path} {destination_path} ", shell=True, stdout = subprocess.DEVNULL)

        return file_names

    def scroll_to_text(self, text):
        str1 = "new UiScrollable(new UiSelector().scrollable(true)."
        str2 = f"instance(0)).scrollIntoView(new UiSelector().text(\"{text}\").instance(0))"
        user_scroll = str1 + str2
        user = self.driver.find_element_by_android_uiautomator(user_scroll)
