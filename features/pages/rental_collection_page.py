from features.pages.base_page import Page
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait


class RentalCollection(Page):
    SCREEN_HEADER_TITLE = (By.XPATH, "//android.widget.TextView[@text = 'Rental Collection']")
    RENTAL_COLLECTION_LIST = (By.XPATH, "//android.view.ViewGroup[@content-desc='rentalCollectionDetailBtn']")

    PROPERTY_CONDITION_PHOTOS_DD = (By.XPATH, "(//android.view.ViewGroup[@content-desc='imageDownBtn'])[1]")
    INVOICES_DD = (By.XPATH, "(//android.view.ViewGroup[@content-desc='imageDownBtn'])[2]")
    LOGS_DD = (By.XPATH, "(//android.view.ViewGroup[@content-desc='imageDownBtn'])[3]")

    def is_property_condition_dd_present(self):
        result = self.is_element_present(self.PROPERTY_CONDITION_PHOTOS_DD)
        return result

    def is_invoices_dd_present(self):
        result = self.is_element_present(self.INVOICES_DD)
        return result

    def is_logs_dd_present(self):
        result = self.is_element_present(self.LOGS_DD)
        return result

    def click_on_record(self):
        self.click_on_element(self.RENTAL_COLLECTION_LIST)

    def does_rc_details_page_show(self):
        if self.is_property_condition_dd_present() and \
                self.is_invoices_dd_present() and \
                self.is_logs_dd_present():
            return True
        else:
            return False
