from features.pages.base_page import Page
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait


class ReferFriendPage(Page):
    MY_REFEREES_BTN = (By.XPATH, "//android.widget.TextView[@text = 'My Referees']")
    REFER_SCREEN_TITLE_TXT = (By.XPATH, "//android.widget.TextView[@text = 'Refer']")

    COPY_LINK_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='referFrndCopyLnkBtn']/android.widget.TextView")
    REFER_LINK_TXT = (By.XPATH, "//android.widget.TextView[contains(@text, 'https://speedhome.com/refer')]")

    def does_refer_link_show(self):
        self.swipe_up(2)
        text_should_be = 'https://speedhome.com/refer'
        Wait.wait5s(self)
        element = self.find_element(self.REFER_LINK_TXT)
        print(f"Refer Link : {element.text}")
        if text_should_be in element.text:
            return True
        else:
            return False
