from features.pages.base_page import Page
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait

class CreateListing(Page):
    PROPERTY_PRICE = ""
    PROPERTY_TYPE = ""
    PROPERTY_NAME = ""

    ADD_PROPERTY_BTN = (
        By.XPATH, "//android.view.ViewGroup[@content-desc='myListingScreenAddBtn']/android.view.ViewGroup")
    LISTINGS_TAB = (By.XPATH, "//android.view.ViewGroup[@content-desc='myListingContListBtn']/android.widget.TextView")
    FAVOURITES_TAB = (By.XPATH, "//android.view.ViewGroup[@content-desc='myListingContFavBtn']/android.widget.TextView")

    UPCOMING_APPOINTMENTS_BTN = (
        By.XPATH, "(//android.view.ViewGroup[@content-desc='myListAccBtn'])[1]/android.widget.TextView")
    ACTIVE_LISTING_BTN = (By.XPATH, "(//android.view.ViewGroup[@content-desc='myListAccBtn'])[2]")
    INACTIVE_LISTING_BTN = (
        By.XPATH, "(//android.view.ViewGroup[@content-desc='myListAccBtn'])[3]")
    REJECTED_LISTING_BTN = (
        By.XPATH, "(//android.view.ViewGroup[@content-desc='myListAccBtn'])[4]")

    RENT_PROPERTY_BTN = (By.XPATH, "(//android.view.ViewGroup[@content-desc='myListingScreenListIconBtn'])[1]")
    SELL_PROPERTY_BTN = (By.XPATH, "(//android.view.ViewGroup[@content-desc='myListingScreenListIconBtn'])[2]")

    RENT_PROPERTY_ROOM_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='createListBedBtn']")
    RENT_PROPERTY_LANDED_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='createListHouseBtn']")
    RENT_PROPERTY_HIGH_RISE_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='createListHeighRiseBtn']")

    SELL_PROPERTY_LANDED_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='propSellLandSaleBtn']")
    SELL_PROPERTY_HIGH_RISE_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='PropSellHeighRiseSaleBtn']")

    PROPERTY_DETAILS_3DOTS_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='moreHorizontalBtn']")
    LISTING_DD_EDIT_LISTING_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='myListEditListingBtn']")
    LISTING_DD_ARCHIVE_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='reactivateArchiveBtn2']")
    LISTING_EDIT_LISTING_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='myListingEditListingBtn']")
    LISTING_ARCHIVE_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='reactivateArchiveBtn3']")

    EDIT_LISTING_PROPERTY_NAME_TXT_FIELD = (
        By.XPATH, "//android.widget.EditText[@content-desc='editListingApartmentInput']")
    EDIT_LISTING_ADDRESS_TXT_FIELD = (
        By.XPATH, "(//android.widget.EditText[@content-desc='editListingStateNameInput'])[1]")
    EDIT_LISTING_POSTAL_CODE_TXT_FIELD = (
        By.XPATH, "(//android.widget.EditText[@content-desc='editListingStateNameInput'])[2]")
    EDIT_LISTING_FLOOR_LEVEL_TXT_FIELD = (
        By.XPATH, "(//android.widget.EditText[@content-desc='editListingStateNameInput'])[3]")
    EDIT_LISTING_PAX_NUMBER_DROPDOWN = (
        By.XPATH, "//android.view.ViewGroup[@content-desc='editScreenDownBtn']/android.widget.TextView[1]")
    EDIT_LISTING_PRICE_TXT_FIELD = (
        By.XPATH, "(//android.widget.EditText[@content-desc='editListingStateNameInput'])[2]")
    EDIT_LISTING_ROOM_TYPE_DROPDOWN = (
        By.XPATH, "//android.view.ViewGroup[@content-desc='editScreenRoomTypeBtn']/android.widget.TextView[1]")
    EDIT_LISTING_BATHROOM_DROPDOWN = (
        By.XPATH, "//android.view.ViewGroup[@content-desc='editScreenBathBtn']/android.widget.TextView[1]")
    EDIT_LISTING_PARKING_DROPDOWN = (
        By.XPATH, "//android.view.ViewGroup[@content-desc='editScreenParkBtn']/android.widget.TextView[1]")
    EDIT_LISTING_FURNISHING_DROPDOWN = (
        By.XPATH, "//android.view.ViewGroup[@content-desc='editScreenFurnBtn']/android.widget.TextView[1]")
    EDIT_LISTING_NEXT_BTN = (
        By.XPATH, "//android.view.ViewGroup[@content-desc='editScreenNextBtn']")
    EDIT_LISTING_BUILD_SIZE_TXT_FIELD = (
        By.XPATH, "(//android.widget.EditText[@content-desc='editListingStateNameInput'])[1]")
    EDIT_LISTING_TITLE_TYPE_DROPDOWN = (
        By.XPATH, "//android.view.ViewGroup[@content-desc='editScreenTitleTypeBtn']/android.widget.TextView[1]")
    EDIT_LISTING_WHOLE_UNIT_FLOOR_LEVEL_DROPDOWN = (
        By.XPATH, "//android.view.ViewGroup[@content-desc='editScreenDownBtn']/android.widget.TextView[1]")
    EDIT_LISTING_BEDROOM_DROPDOWN = (
        By.XPATH, "//android.view.ViewGroup[@content-desc='editScreenBedBtn']/android.widget.TextView[1]")

    SHOWN_LISTING_PRICE = (
    By.XPATH, "//android.view.ViewGroup[@content-desc='myListingSliderBtn2']/android.widget.TextView[1]")
    SHOWN_LISTING_TYPE = (
    By.XPATH, "//android.view.ViewGroup[@content-desc='myListingSliderBtn2']/android.widget.TextView[2]")
    SHOWN_LISTING_NAME = (
    By.XPATH, "//android.view.ViewGroup[@content-desc='myListingSliderBtn2']/android.widget.TextView[3]")

    UPDATE_STATUS_BOX_TITLE = (By.XPATH, "//android.widget.TextView[@text = 'Update Status']")
    UPDATE_STATUS_BOX_DESC = (
    By.XPATH, "//android.widget.TextView[contains(@text, 'change status of this property ?')]")
    UPDATE_STATUS_BOX_OK_BTN = (By.XPATH, "//android.widget.Button[@text = 'OK']")
    UPDATE_STATUS_BOX_CANCEL_BTN = (By.XPATH, "//android.widget.Button[@text = 'CANCEL']")

    def is_add_property_btn_present(self):
        result = self.is_element_present(self.ADD_PROPERTY_BTN)
        return result

    def click_on_add_property_btn(self):
        self.click_on_element(self.ADD_PROPERTY_BTN)
        Wait.wait2s(self)

    def is_rent_property_btn_present(self):
        result = self.is_element_present(self.RENT_PROPERTY_BTN)
        return result

    def is_sell_property_btn_present(self):
        result = self.is_element_present(self.SELL_PROPERTY_BTN)
        return result

    def click_sell_property_btn(self):
        self.click_on_element(self.SELL_PROPERTY_BTN)
        Wait.wait2s(self)

    def click_rent_property_btn(self):
        self.click_on_element(self.RENT_PROPERTY_BTN)
        Wait.wait2s(self)

    def does_rent_sell_btn_show(self):
        if self.is_rent_property_btn_present() and self.is_sell_property_btn_present():
            return True
        else:
            return False

    def is_rent_property_room_btn_present(self):
        result = self.is_element_present(self.RENT_PROPERTY_ROOM_BTN)
        return result

    def is_rent_property_landed_btn_present(self):
        result = self.is_element_present(self.RENT_PROPERTY_LANDED_BTN)
        return result

    def is_rent_property_high_rise_btn_present(self):
        result = self.is_element_present(self.RENT_PROPERTY_HIGH_RISE_BTN)
        return result

    def is_sell_property_landed_btn_present(self):
        result = self.is_element_present(self.SELL_PROPERTY_LANDED_BTN)
        return result

    def is_sell_property_high_rise_btn_present(self):
        result = self.is_element_present(self.SELL_PROPERTY_HIGH_RISE_BTN)
        return result

    def does_all_rent_property_options_show(self):
        if self.is_rent_property_room_btn_present() and \
                self.is_rent_property_landed_btn_present() and \
                self.is_rent_property_high_rise_btn_present():
            return True
        else:
            return False

    def does_all_sell_property_options_show(self):
        if self.is_sell_property_landed_btn_present() and \
                self.is_sell_property_high_rise_btn_present():
            return True
        else:
            return False

    def click_active_listing_btn(self):
        self.click_on_element(self.ACTIVE_LISTING_BTN)
        Wait.wait2s(self)
        self.save_first_listing_data()

    def click_inactive_listing_btn(self):
        self.click_on_element(self.INACTIVE_LISTING_BTN)
        Wait.wait2s(self)
        self.save_first_listing_data()

    def click_edit_listing_btn(self):
        pro_type = self.PROPERTY_TYPE
        #if "Room" in pro_type:
        if self.is_element_present(self.PROPERTY_DETAILS_3DOTS_BTN):
            self.click_on_element(self.PROPERTY_DETAILS_3DOTS_BTN)
            Wait.wait2s(self)
            self.click_on_element(self.LISTING_DD_EDIT_LISTING_BTN)
            Wait.wait2s(self)

        else:
            self.scroll_to_text("Edit Listing")
            self.click_on_element(self.LISTING_EDIT_LISTING_BTN)
            Wait.wait2s(self)

    def click_archive_listing_btn(self):
        pro_type = self.PROPERTY_TYPE
        # if "Room" in pro_type:
        if self.is_element_present(self.PROPERTY_DETAILS_3DOTS_BTN):
            self.click_on_element(self.PROPERTY_DETAILS_3DOTS_BTN)
            Wait.wait2s(self)
            self.click_on_element(self.LISTING_DD_ARCHIVE_BTN)
            Wait.wait2s(self)

        else:
            self.scroll_to_text("Archive")
            self.click_on_element(self.LISTING_ARCHIVE_BTN)
            Wait.wait2s(self)

    def click_ok_on_archive_status_popup(self):
        self.click_on_element(self.UPDATE_STATUS_BOX_OK_BTN)
        Wait.wait2s(self)

    def is_edit_property_name_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_PROPERTY_NAME_TXT_FIELD)
        return result

    def is_edit_property_address_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_ADDRESS_TXT_FIELD)
        return result

    def is_edit_property_postcode_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_POSTAL_CODE_TXT_FIELD)
        return result

    def is_edit_property_floorlevel_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_FLOOR_LEVEL_TXT_FIELD)
        return result

    def is_edit_property_pax_number_present(self):
        self.swipe_up(1)
        result = self.is_element_txt_present(self.EDIT_LISTING_PAX_NUMBER_DROPDOWN)
        return result

    def is_edit_property_room_type_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_ROOM_TYPE_DROPDOWN)
        return result

    def is_edit_property_bathroom_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_BATHROOM_DROPDOWN)
        return result

    def is_edit_property_parking_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_PARKING_DROPDOWN)
        return result

    def is_edit_property_furnishing_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_FURNISHING_DROPDOWN)
        return result

    def is_edit_property_price_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_PRICE_TXT_FIELD)
        return result

    def is_edit_property_next_btn_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_NEXT_BTN)
        return result

    def is_edit_property_build_size_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_BUILD_SIZE_TXT_FIELD)
        return result

    def is_edit_property_title_type_present(self):
        self.swipe_up(1)
        result = self.is_element_txt_present(self.EDIT_LISTING_TITLE_TYPE_DROPDOWN)
        return result

    def is_edit_property_wh_floor_level_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_WHOLE_UNIT_FLOOR_LEVEL_DROPDOWN)
        return result

    def is_edit_property_bedroom_present(self):
        result = self.is_element_txt_present(self.EDIT_LISTING_BEDROOM_DROPDOWN)
        return result

    def does_edit_property_all_fields_show(self):
        all_fields_exist = False
        pro_type = self.PROPERTY_TYPE
        if "Room" in pro_type:
            if self.is_edit_property_name_present() and \
                    self.is_edit_property_address_present() and \
                    self.is_edit_property_postcode_present() and \
                    self.is_edit_property_pax_number_present() and \
                    self.is_edit_property_room_type_present() and \
                    self.is_edit_property_bathroom_present() and \
                    self.is_edit_property_parking_present() and \
                    self.is_edit_property_furnishing_present() and \
                    self.is_edit_property_price_present():
                all_fields_exist = True

        else:
            if self.is_edit_property_name_present() and \
                    self.is_edit_property_address_present() and \
                    self.is_edit_property_postcode_present() and \
                    self.is_edit_property_build_size_present() and \
                    self.is_edit_property_title_type_present() and \
                    self.is_edit_property_bedroom_present() and \
                    self.is_edit_property_bathroom_present() and \
                    self.is_edit_property_parking_present() and \
                    self.is_edit_property_furnishing_present() and \
                    self.is_edit_property_price_present():
                all_fields_exist = True

        return all_fields_exist

    def save_first_listing_data(self):
        self.PROPERTY_PRICE = self.get_element_txt(self.SHOWN_LISTING_PRICE)
        self.PROPERTY_TYPE = self.get_element_txt(self.SHOWN_LISTING_TYPE)
        self.PROPERTY_NAME = self.get_element_txt(self.SHOWN_LISTING_NAME)

    def does_archived_property_shows_inactive(self):
        name = self.PROPERTY_NAME
        typee = self.PROPERTY_TYPE
        price = self.PROPERTY_PRICE

        self.click_inactive_listing_btn()
        self.scroll_to_text(name)
        self.save_first_listing_data()

        inactive_name = self.PROPERTY_NAME
        inactive_type = self.PROPERTY_TYPE
        inactive_price = self.PROPERTY_PRICE

        if name == inactive_name and typee == inactive_type and price == inactive_price:
            return True
        else:
            return False
