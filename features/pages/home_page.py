from selenium.webdriver.remote.webelement import WebElement

from features.pages.base_page import Page
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait


class HomePage(Page):
    BACK_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='numberBackBtn']")
    SEARCH_TXT_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='editTextHomeSearchPropertyTextBox']")
    SEARCH_ICON = (By.XPATH, "//android.view.ViewGroup[@content-desc='editTextHomeSearchIconBtn']")
    SEARCH_DROP_DOWN_TXT = (By.XPATH, "//android.view.ViewGroup[@content-desc='homeImageBtn']/android.widget.TextView")
    SEARCH_DROP_DOWN_ICON = (
        By.XPATH,
        "//android.view.ViewGroup[@content-desc='homeImageBtn']/android.view.ViewGroup/android.widget.TextView")
    SEARCH_DD_BUY_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='homeBuyBtn']")
    SEARCH_DD_RENT_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='homeRentBtn']")
    HOME_BTN = (By.XPATH, "//android.view.ViewGroup/android.widget.Button[1]")
    CHAT_BTN = (By.XPATH, "//android.view.ViewGroup/android.widget.Button[2]")
    PROPERTIES_BTN = (By.XPATH, "//android.view.ViewGroup/android.widget.Button[3]")
    MORE_BTN = (By.XPATH, "//android.view.ViewGroup/android.widget.Button[4]")
    UP_TO_DATE_BANNER = (
        By.XPATH, "//android.view.ViewGroup[@content-desc='homeUpdateBannerBtn']/android.view.ViewGroup")

    def is_searchfield_present(self):
        result = self.is_element_present(self.SEARCH_TXT_FIELD)
        return result

    def is_home_btn_present(self):
        result = self.is_element_present(self.HOME_BTN)
        return result

    def is_chat_btn_present(self):
        result = self.is_element_present(self.CHAT_BTN)
        return result

    def is_properties_btn_present(self):
        result = self.is_element_present(self.PROPERTIES_BTN)
        return result

    def is_more_btn_present(self):
        result = self.is_element_present(self.MORE_BTN)
        return result

    def does_home_page_show(self):
        if self.is_searchfield_present() and self.is_home_btn_present() and self.is_more_btn_present():
            return True
        else:
            return False

    def click_on_more_btn(self):
        self.click_on_element(self.MORE_BTN)
        Wait.wait2s(self)

    def click_on_properties_btn(self):
        self.click_on_element(self.PROPERTIES_BTN)
        Wait.wait3s(self)

    def click_on_home_btn(self):
        self.click_on_element(self.HOME_BTN)
        Wait.wait3s(self)

    def click_search_dd_icon(self):
        self.click_on_element(self.SEARCH_DROP_DOWN_ICON)
        Wait.wait2s(self)

    def click_search_icon(self):
        self.click_on_element(self.SEARCH_ICON)
        Wait.wait2s(self)

    def click_search_buy_btn(self):
        self.click_search_dd_icon()
        self.click_on_element(self.SEARCH_DD_BUY_BTN)

        Wait.wait2s(self)

        self.click_search_icon()

    def click_search_rent_btn(self):
        self.click_search_dd_icon()
        self.click_on_element(self.SEARCH_DD_RENT_BTN)

        Wait.wait2s(self)

        self.click_search_icon()

    def is_dd_buy_option_present(self):
        result = self.is_element_present(self.SEARCH_DD_BUY_BTN)
        return result

    def is_dd_rent_option_present(self):
        result = self.is_element_present(self.SEARCH_DD_RENT_BTN)
        return result

    def are_both_searchfield_options_present(self):
        self.click_search_dd_icon()

        if self.is_dd_buy_option_present() and self.is_dd_rent_option_present():
            return True
        else:
            return False

    def is_searchbar_with_all_options_present(self):
        if self.is_searchfield_present() and self.are_both_searchfield_options_present():
            return True
        else:
            return False

    # def show_details(self):
    #     self.swipe_up(1)
    #     Wait.wait2s(self)
    #     # first_card = self.driver.find_element_by_xpath("(//android.view.ViewGroup[@content-desc='propCardSliderBtn'])[1]")
    #     first_card = self.find_element(self.HP_FIRST_PROPERTY_CARD)
    #     print(f"Total Elements : {first_card.text}")
    #     PRICE_XPATH = (By.XPATH, "//android.widget.TextView[1]")
    #     TYPE_XPATH = (By.XPATH, "//android.widget.TextView[2]")
    #     NAME_XPATH = (By.XPATH, "//android.widget.TextView[3]")
    #
    #     PRICE_ELE = first_card.find_element(by=PRICE_XPATH[0],
    #                                         value=PRICE_XPATH[1]).text
    #
    #     TYPE_ELE = first_card.find_element(by=TYPE_XPATH[0],
    #                                        value=TYPE_XPATH[1]).text
    #
    #     NAME_ELE = first_card.find_element(by=NAME_XPATH[0],
    #                                        value=NAME_XPATH[1]).text
    #
    #     print(f"NAME FOUND : {NAME_ELE}")
    #     print(f"TYPE FOUND : {TYPE_ELE}")
    #     print(f"PRICE FOUND : {PRICE_ELE}")
    #
    #     self.NAME_TEST = NAME_ELE
    #     self.TYPE_TEST = TYPE_ELE
    #     self.PRICE_TEST = PRICE_ELE
