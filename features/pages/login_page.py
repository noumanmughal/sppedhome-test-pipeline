from features.pages.base_page import Page
from selenium.webdriver.common.by import By
from features.utils.Wait import Wait


class LoginPage(Page):

    BACK_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='numberBackBtn']")
    COUNTRY_CODE_TXT_FIELD = (By.XPATH, "(//android.widget.EditText)[1]")
    ENTER_MOBILE_NUMBER_TEXT_TITLE = (By.XPATH, "//android.widget.TextView[contains(@text, 'Enter Your')]")
    YOUR_MOBILE_NUMBER_TXT_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='numbmerScreenMobInput']")
    NEXT_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='numberCompNextBtn']")
    WELCOME_MODAL_TXT = (By.XPATH, "//android.widget.TextView[@text = 'Welcome']")
    WELCOME_MODAL_REQUEST_OTP_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Request 4 digit OTP']")
    WELCOME_MODAL_LOGIN_WITH_PASSWORD_BTN = (By.XPATH, "//android.widget.TextView[@text = 'Login with password']")
    ONE_TIME_PASSWORD_TITLE_TXT = (By.XPATH, "//android.view.ViewGroup[@content-desc='otpPassBtn']/android.widget.TextView")
    OTP_NEXT_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='otpVerifyBtn']")
    OTP_TXT_FIELD = (By.XPATH, "//android.widget.EditText")
    EDIT_TEXT_FIELDS = (By.XPATH, "(//android.widget.EditText)[1]")
    COUNTRY_FLAG_IMAGE = (By.XPATH, "//android.widget.ImageView")
    COUNTRY_LIST = (By.XPATH, "//android.widget.ScrollView[2]")
    LETTER_U = (By.XPATH, "//android.widget.TextView[@text = 'U']")
    US_IN_COUNTRY_LIST = (By.XPATH, "//android.widget.TextView[@text = 'United States']")
    OTP_SENT_TOAST_MSG = (By.XPATH, "//android.widget.TextView[@text = 'OTP sent successfully']")
    OTP_TIMER = (By.XPATH, "//android.widget.TextView[2]")
    DIDNT_GET_CODE_TXT = (By.XPATH, "//android.widget.TextView[contains(@text, 'get the code?')]")
    RESEND_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='otpResentBtn']")
    INVALID_OTP_ERR_MSG = (By.XPATH, "//android.widget.TextView[@text = 'Invalid OTP numbers entered. Try again.']")

    ENTER_PASSWORD_SCREEN_TITLE = (By.XPATH, "//android.view.ViewGroup[@content-desc='passwordPassBtn']/android.widget.TextView")
    ENTER_PASSWORD_TXT_FIELD = (By.XPATH, "//android.widget.EditText[@content-desc='confirmScreenPassInput']")
    FORGOT_PASSWORD_BTN = (By.XPATH, "//android.widget.TextView[contains(@text, 'Forget Password?')]")
    ENTER_PASSWORD_NEXT_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='passwordVerifyBtn']")

    NEW_PASSWORD_SCREEN_TITLE = (By.XPATH, "//android.view.ViewGroup[@content-desc='passwordPassBtn']/android.widget.TextView")
    NEW_PASSWORD_TXT_FIELD = (By.XPATH, "(//android.widget.EditText[@content-desc='confirmScreenPassInput'])[1]")
    NEW_CONFIRM_PASSWORD_TXT_FIELD = (By.XPATH, "(//android.widget.EditText[@content-desc='confirmScreenPassInput'])[2]")
    NEW_PASSWORD_NEXT_BTN = (By.XPATH, "//android.view.ViewGroup[@content-desc='passwordVerifyBtn']")



    # def login_with_email_and_password(self):
    #
    #     if not EMAIL or not PASSWORD:
    #         raise Exception("Please add credentials to configuration file!")
    #
    #     self.input(EMAIL, self.EMAIL_INPUT)
    #     self.input(PASSWORD, self.PASSWORD_INPUT)
    #
    #     self.click_on_element(self.SUBMIT_BUTTON)

    def is_login_screen_title_present(self):
        result = self.is_element_present(self.ENTER_MOBILE_NUMBER_TEXT_TITLE)
        return result

    def is_country_code_txtfield_present(self):
        result = self.is_element_present(self.COUNTRY_CODE_TXT_FIELD)
        return result

    def is_mobile_numbertxtfielld_present(self):
        result = self.is_element_present(self.YOUR_MOBILE_NUMBER_TXT_FIELD)
        return result

    def is_next_btn_present(self):
        result = self.is_element_present(self.NEXT_BTN)
        return result

    def enter_unreg_mobile_number(self, mobile_number):
        self.input(mobile_number, self.YOUR_MOBILE_NUMBER_TXT_FIELD)
        Wait.wait1s(self)

    def enter_registered_mobile_number(self, mobile_number):
        self.input(mobile_number, self.YOUR_MOBILE_NUMBER_TXT_FIELD)
        Wait.wait1s(self)

    def click_next_btn(self):
        self.click_on_element(self.NEXT_BTN)
        # Wait.wait5s(self)

    def is_welcome_modal_txt_shown(self):
        result = self.is_element_present(self.WELCOME_MODAL_TXT)
        return result

    def is_request_otp_button_shown(self):
        result = self.is_element_present(self.WELCOME_MODAL_REQUEST_OTP_BTN)
        return result

    def is_login_with_pass_button_shown(self):
        result = self.is_element_present(self.WELCOME_MODAL_LOGIN_WITH_PASSWORD_BTN)
        return result

    def is_unreg_welcome_modal_shown(self):
        if self.is_welcome_modal_txt_shown() and self.is_request_otp_button_shown():
            return True
        else:
            return False

    def is_reg_welcome_modal_shown(self):
        if self.is_welcome_modal_txt_shown() and self.is_request_otp_button_shown() and self.is_login_with_pass_button_shown():
            return True
        else:
            return False

    def click_on_request_otp(self):
        self.click_on_element(self.WELCOME_MODAL_REQUEST_OTP_BTN)
        Wait.wait5s(self)

    def click_on_login_with_password(self):
        self.click_on_element(self.WELCOME_MODAL_LOGIN_WITH_PASSWORD_BTN)
        Wait.wait3s(self)

    def is_otp_title_present(self):
        result = self.is_element_present(self.ONE_TIME_PASSWORD_TITLE_TXT)
        return result

    def is_otp_txtfield_present(self):
        result = self.is_element_present(self.OTP_TXT_FIELD)
        return result

    def is_otp_next_btn_present(self):
        result = self.is_element_present(self.OTP_NEXT_BTN)
        return result

    def is_otp_modal_shown(self):
        if self.is_otp_title_present() and self.is_otp_txtfield_present() and self.is_otp_next_btn_present():
            return True
        else:
            return False

    def enter_otp(self, otp_code):
        self.input(otp_code, self.OTP_TXT_FIELD)
        Wait.wait5s(self)

    def enter_password(self, password):
        self.input(password, self.ENTER_PASSWORD_TXT_FIELD)
        Wait.wait2s(self)

    def click_on_enter_password_next_btn(self):
        self.click_on_element(self.ENTER_PASSWORD_NEXT_BTN)
        Wait.wait2s(self)

    def click_on_forgot_password_btn(self):
        self.click_on_element(self.FORGOT_PASSWORD_BTN)
        Wait.wait2s(self)

    def does_mobile_number_has_placeholder_txt(self):
        element = self.find_element(self.YOUR_MOBILE_NUMBER_TXT_FIELD)
        if "Your Mobile Number" in element.text:
            return True
        else:
            return False

    def do_some_testing(self):
        self.click_on_element(self.COUNTRY_FLAG_IMAGE)
        Wait.wait2s(self)
        self.click_on_element(self.LETTER_U)
        Wait.wait5s(self)
        self.click_on_element(self.US_IN_COUNTRY_LIST)
        Wait.wait5s(self)

    def is_otp_timer_present(self):
        result = self.is_element_present(self.OTP_TIMER)
        return result

    def is_otp_timer_not_present(self):
        result = self.is_element_not_present(self.OTP_TIMER)
        return result

    def wait_for_otp_timer_to_expire(self):
        Wait.waitFor(self, 60)

    def is_didnt_get_code_txt_present(self):
        result = self.is_element_present(self.DIDNT_GET_CODE_TXT)
        return result

    def resend_btn_present(self):
        result = self.is_element_present(self.RESEND_BTN)
        return result

    def click_on_resend_btn(self):
        self.click_on_element(self.RESEND_BTN)

    def is_otp_sent_toast_message_present(self):
        result = self.is_element_present(self.OTP_SENT_TOAST_MSG)
        return result

    def accepted_otp_code(self):
        text_showing = ''
        elements = self.find_elements(self.OTP_TXT_FIELD)
        for element in elements:
            text_showing += element.text

        return text_showing

    def is_invalid_otp_err_present(self):
        result = self.is_element_present(self.INVALID_OTP_ERR_MSG)
        return result

    def is_reset_password_title_present(self):
        result = self.is_element_present(self.NEW_PASSWORD_SCREEN_TITLE)
        return result

    def is_new_password_txtfield_present(self):
        result = self.is_element_present(self.NEW_PASSWORD_TXT_FIELD)
        return result

    def is_new_password_next_btn_present(self):
        result = self.is_element_present(self.NEW_PASSWORD_NEXT_BTN)
        return result

    def is_reset_password_screen_shown(self):
        if self.is_reset_password_title_present() and self.is_new_password_txtfield_present() and self.is_new_password_next_btn_present():
            return True
        else:
            return False
















