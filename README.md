# SpeedHome Automation Suite
## Python, Behave &amp; Appium

#### Tools:
- Appium : https://appium.io/downloads.html
- Android Emulator : https://developer.android.com/studio
- IDE (PyCharm) : https://www.jetbrains.com/pycharm/

#### Setup:
1. Start the Appium Server 
2. Start the Android Emulator

#### How To Start Appium Server:
https://digital.ai/catalyst-blog/what-is-appium-server-a-complete-end-to-end-guide

#### How To Start Android Emulator:
https://www.alphr.com/run-android-emulator/

#### Setup:
1. Clone the repo
2. In terminal or cmd go to the project root directory
3. Install the virtualenv using command `pip install virtualenv`
4. Create virtual environment by running following command`virtualenv venv`
5. Activate the environment `venv\Scripts\activate`
6. Install the packages using command `pip install -r requirements.txt`

#### How to run:
1. Go to project root folder
2. Add mobile number and static otp code to the config file (/features/credentials.py)
3. Update desired capabilities for Android (/features/environment.py)
4. Place the `speedhome.apk` in the "resources" folder at root 
5. Execute command: `behave`, this will run all the feature files. No report will be generated. After the testrun summary will be displayed

#### How to Run and Generate Report Method 1 (script): Allure + Screenshots
- For Test Execute and report generation execute the following command in the IDE terminal
 `run.sh`

#### How to Run and Generate Report Method 2: Allure + Screenshots
- Empty the `allure/results` folder

- For Test Execute and report generation execute the following command
 `behave -f allure_behave.formatter:AllureFormatter -o ./allure/results/ ./features/`
  

- To specify the tags execute the following command
 `behave -f allure_behave.formatter:AllureFormatter -o ./allure/results/ ./features/ --tags=registration`
  "behave-repot.html" will be created at project root directory


- To view the report execute the following command, browser window will open automatically
 `allure serve ./allure/results/`
  
#### How to Run and Generate Report : Behave HTML Report (simple)
- For Test Execute and report generation execute the following command
 `behave -f html -o behave-report.html`
  
#### How to Run With only console logs 
- Execute the following command
 `behave`
  
#### Result:
TO BE ADDED


#### Environment:
- Windows 10
- Python 3.9.6 
- appium 1.21.0
- IDE : Pycharm